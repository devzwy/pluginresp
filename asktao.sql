/*
 Navicat Premium Data Transfer

 Source Server         : 1.116.54.237
 Source Server Type    : MySQL
 Source Server Version : 50735
 Source Host           : 1.116.54.237:3306
 Source Schema         : asktao

 Target Server Type    : MySQL
 Target Server Version : 50735
 File Encoding         : 65001

 Date: 30/09/2021 16:46:49
*/

CREATE DATABASE IF NOT EXISTS asktao;

use asktao;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ACTIVE_RESET_LOG
-- ----------------------------
DROP TABLE IF EXISTS `ACTIVE_RESET_LOG`;
CREATE TABLE `ACTIVE_RESET_LOG`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键自增',
                                     `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '玩家账号',
                                     `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                     `role_gid` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '角色GID',
                                     `jin` int(255) NULL DEFAULT NULL COMMENT '消耗的金',
                                     `yin` int(255) NULL DEFAULT NULL COMMENT '消耗的银元宝',
                                     `do_type` int(10) NULL DEFAULT NULL COMMENT '操作类型 0 法宝共生 ',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
                                     `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ACTIVE_RESET_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_ACTIVE_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_ACTIVE_LOG`;
CREATE TABLE `GAME_ACTIVE_LOG`  (
                                    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                    `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '玩家账号',
                                    `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                    `role_gid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色GID',
                                    `gift_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖励类型',
                                    `jin` int(255) NULL DEFAULT NULL COMMENT '领取元宝数量',
                                    `yin` int(255) NULL DEFAULT NULL COMMENT '领取银元宝数量',
                                    `prop_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '道具名称',
                                    `prop_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '道具ID',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_ACTIVE_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_CDK
-- ----------------------------
DROP TABLE IF EXISTS `GAME_CDK`;
CREATE TABLE `GAME_CDK`  (
                             `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                             `cdk` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'CDK',
                             `status` int(11) NOT NULL DEFAULT 0 COMMENT '使用状态 0未使用 1 已使用',
                             `create_time` datetime(0) NOT NULL COMMENT '创建时间',
                             `use_time` datetime(0) NULL DEFAULT NULL COMMENT '使用时间',
                             `use_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用者用户账号',
                             `cdk_dec` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'CDK描述',
                             `cdk_type` int(11) NOT NULL COMMENT 'cdk类型',
                             `bindData` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cdk绑定的道具名称，当类型为金银元宝时这里填写金银元宝数量',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 237534 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_CDK
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_LEVEL_UP_GIFT_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_LEVEL_UP_GIFT_LOG`;
CREATE TABLE `GAME_LEVEL_UP_GIFT_LOG`  (
                                           `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                           `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '领取账号',
                                           `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
                                           `jin` int(255) NULL DEFAULT NULL COMMENT '发放金元宝',
                                           `yin` int(255) NULL DEFAULT NULL COMMENT '发放银元宝',
                                           `create_time` datetime(0) NULL DEFAULT NULL COMMENT '领取时间',
                                           `level` int(255) NULL DEFAULT NULL COMMENT '奖励等级',
                                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_LEVEL_UP_GIFT_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_LOG_CDK
-- ----------------------------
DROP TABLE IF EXISTS `GAME_LOG_CDK`;
CREATE TABLE `GAME_LOG_CDK`  (
                                 `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                 `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '使用者账号',
                                 `cdk` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '使用的CDK',
                                 `create_time` datetime(0) NOT NULL COMMENT '使用时间',
                                 `use_type` int(11) NOT NULL COMMENT '使用类型',
                                 `cdk_des` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'CDK描述',
                                 `bind_data` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '绑定数据',
                                 `cdk_type` int(11) NOT NULL COMMENT 'cdk类型',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_LOG_CDK
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_LUCK_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_LUCK_LOG`;
CREATE TABLE `GAME_LUCK_LOG`  (
                                  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抽奖用户',
                                  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                  `luck_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品名称',
                                  `luck_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品商城编码',
                                  `luck_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品类别',
                                  `do_type` int(1) NULL DEFAULT NULL COMMENT '抽奖类型',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '抽奖时间',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_LUCK_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_PAODIAN_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_PAODIAN_LOG`;
CREATE TABLE `GAME_PAODIAN_LOG`  (
                                     `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                     `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'account',
                                     `status` int(1) NULL DEFAULT NULL COMMENT 'status',
                                     `jin` int(255) NULL DEFAULT NULL COMMENT 'jin',
                                     `yin` int(255) NULL DEFAULT NULL COMMENT 'yin',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT 'create_time',
                                     PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 481 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_PAODIAN_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_RECALL_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_RECALL_LOG`;
CREATE TABLE `GAME_RECALL_LOG`  (
                                    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                    `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '玩家账号',
                                    `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                    `role_gid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色GID',
                                    `item_type` int(1) NULL DEFAULT NULL COMMENT '回收类型',
                                    `item_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '道具名称',
                                    `recall_number` int(11) NULL DEFAULT NULL COMMENT '回收数量',
                                    `jin` int(255) NULL DEFAULT NULL COMMENT '金元宝数量',
                                    `yin` int(255) NULL DEFAULT NULL COMMENT '银元宝数量',
                                    `create_time` datetime(0) NULL DEFAULT NULL COMMENT '回收时间',
                                    `exec_time` int(11) NULL DEFAULT NULL,
                                    `exec_status` int(1) NULL DEFAULT NULL,
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_RECALL_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for GAME_XINFA_LOG
-- ----------------------------
DROP TABLE IF EXISTS `GAME_XINFA_LOG`;
CREATE TABLE `GAME_XINFA_LOG`  (
                                   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                   `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
                                   `role_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                   `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
                                   `do_type` int(1) NULL DEFAULT NULL COMMENT '处罚类型',
                                   `create_time` datetime(0) NULL DEFAULT NULL COMMENT '触发时间',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 186 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of GAME_XINFA_LOG
-- ----------------------------

-- ----------------------------
-- Table structure for gateway_log
-- ----------------------------
DROP TABLE IF EXISTS `gateway_log`;
CREATE TABLE `gateway_log`  (
                                `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                                `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册账号',
                                `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
                                `device_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备码',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
                                `reg_qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册QQ',
                                `role_gid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色GID',
                                `polar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '五行属性',
                                `role_sex` int(255) NULL DEFAULT NULL COMMENT '角色性别',
                                `reg_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册ip',
                                PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 169 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gateway_log
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
                              `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                              `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
                              `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
                              `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
                              `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
                              `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
                              `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
                              `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
                              `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
                              `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
                              `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
                              `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
                              `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
                              `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
                              `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
                              `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                              `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                              `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                              `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                              PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
                                     `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                                     `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
                                     `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
                                     `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
                                     `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
                                     `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
                                     `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
                                     `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
                                     `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
                                     `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
                                     `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
                                     `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
                                     `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
                                     `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
                                     `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
                                     `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
                                     `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                     `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                     `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                     `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                     `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                     PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 310 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
                               `config_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
                               `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
                               `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
                               `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
                               `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
                               `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                               PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-08-22 18:51:29', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-08-22 18:51:29', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-08-22 18:51:29', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2021-08-22 18:51:29', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-08-22 18:51:29', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
                             `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
                             `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                             `order_num` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
                             `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
                             `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
                             `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 110 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
                                  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
                                  `dict_sort` int(11) NULL DEFAULT 0 COMMENT '字典排序',
                                  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
                                  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
                                  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
                                  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
                                  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
                                  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 244 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (100, 1, '未使用', '0', 'game_cdk_status', NULL, 'default', 'N', '0', 'admin', '2021-08-25 11:16:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (101, 2, '已使用', '1', 'game_cdk_status', NULL, 'default', 'N', '0', 'admin', '2021-08-25 11:17:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (102, 1, '兑换道具', '0', 'game_cdk_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 11:17:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (103, 2, '兑换金元宝', '1', 'game_cdk_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 11:18:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 3, '兑换银元宝', '2', 'game_cdk_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 11:18:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 1, '游戏中使用', '0', 'cdk_use_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 13:50:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (106, 2, '登录器使用', '1', 'cdk_use_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 13:50:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (107, 1, '不绑定', '0', 'game_prop_bind_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 15:40:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 2, '绑定', '1', 'game_prop_bind_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 15:40:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 3, '永久绑定', '2', 'game_prop_bind_type', NULL, 'default', 'N', '0', 'admin', '2021-08-25 15:41:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 2, '正常', '1', 'game_server_line_status', NULL, 'default', 'N', '0', 'admin', '2021-08-26 11:09:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 1, '停用', '0', 'game_server_line_status', NULL, 'default', 'N', '0', 'admin', '2021-08-26 11:09:34', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 1, '普通玩家', '0', 'role_pri_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:22:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 1, 'GM(200)', '200', 'role_pri_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:23:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 3, 'GM(300)', '300', 'role_pri_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:23:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 4, 'GM(1000)', '1000', 'role_pri_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:23:26', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (116, 1, '物品丢弃', 'drop', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:50:41', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (117, 2, '宠物丢弃', 'drop_pet', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:50:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (118, 3, '物品拾取', 'get', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:51:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 4, '金钱交易', 'buy', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:51:56', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (120, 5, '死亡惩罚', 'penalty', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 10:52:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (121, 1, '金元宝', '0', 'game_cost_platform', NULL, 'default', 'N', '0', 'admin', '2021-08-27 11:31:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (122, 2, '银元宝', '1', 'game_cost_platform', NULL, 'default', 'N', '0', 'admin', '2021-08-27 11:31:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (123, 1, '属性修改', '92', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-27 12:09:42', 'admin', '2021-08-28 16:26:29', NULL);
INSERT INTO `sys_dict_data` VALUES (124, 6, '死亡记录', 'die', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 14:34:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (125, 1, '物品', '1', 'game_recall_miaoshou_recal_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 14:52:35', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (126, 2, '宠物', '0', 'game_recall_miaoshou_recal_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 14:52:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (127, 2, '已领取', '1', 'game_recall_miaoshou_finish_status', NULL, 'default', 'N', '0', 'admin', '2021-08-28 14:53:17', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (128, 1, '未领取', '0', 'game_recall_miaoshou_finish_status', NULL, 'default', 'N', '0', 'admin', '2021-08-28 14:53:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (129, 2, '属性查看', '3', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:26:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (130, 9, '技能查看', '9', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:28:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (131, 10, 'GID查看', '10', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:29:04', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (132, 107, 'IP查看', '107', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:29:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (133, 121, '玩家查找', '121', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:31:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (134, 77, '客服频道限制', '77', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:32:16', 'admin', '2021-08-28 16:32:50', NULL);
INSERT INTO `sys_dict_data` VALUES (135, 4, '玩家警告', '4', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:33:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (136, 0, '禁言玩家', '114', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:33:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (137, 5, '强剔下线', '5', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:34:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (138, 117, '关入监狱', '117', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:35:35', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (139, 118, '关入天牢', '118', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:36:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (140, 113, '角色封禁', '113', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:37:34', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (141, 8, '监听屏幕', '8', 'gm_do_type', NULL, 'success', 'N', '0', 'admin', '2021-08-28 16:38:12', 'admin', '2021-08-28 19:53:40', NULL);
INSERT INTO `sys_dict_data` VALUES (142, 11, '接近目标', '11', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:45:14', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (143, 7, '抓至身边', '7', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:45:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (144, 109, '外挂检测', '109', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:46:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (145, 2, '账号查询', '2', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:47:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (146, 63, '地图查询', '63', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:47:44', 'admin', '2021-08-28 16:49:22', NULL);
INSERT INTO `sys_dict_data` VALUES (147, 13, 'NPC查询', '13', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:49:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (148, 137, 'Mac数查询', '137', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:50:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (149, 136, 'IP数查询', '136', 'gm_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-28 16:50:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (150, 7, '开始战斗', 'start_combat', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-29 09:07:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (151, 8, '结束战斗', 'end_combat', 'player_log_do_type', NULL, 'default', 'N', '0', 'admin', '2021-08-29 09:07:42', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (152, 1, '金-五龙山云霄洞', '1', 'game_polar', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:12:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (153, 2, '木-钟南山玉柱洞', '2', 'game_polar', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:13:03', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (154, 3, '水-凤凰山斗阙宫', '3', 'game_polar', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:13:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (155, 4, '火-乾元山金光洞', '4', 'game_polar', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:14:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (156, 5, '土-骷髅山白骨洞', '5', 'game_polar', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:15:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (157, 1, '男', '1', 'game_sex', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:16:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (158, 2, '女', '0', 'game_sex', NULL, NULL, 'N', '0', 'admin', '2021-07-28 00:16:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (159, 1, '横幅消息', '1', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:36:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (160, 2, '提示消息', '2', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:36:42', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (161, 3, '系统邮件', '3', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:37:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (162, 4, '喇叭频道', '4', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:37:10', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (163, 5, '当前频道', '5', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:37:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (164, 6, '问道频道', '6', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:37:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (165, 7, '客服频道', '7', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:37:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (166, 8, '维护频道', '8', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:39:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (167, 9, '活动频道', '9', 'game_message_type', NULL, NULL, 'N', '0', 'admin', '2021-07-29 15:40:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (168, 4, '赠送', '3', 'game_prop_bind_type', NULL, 'default', 'N', '0', 'admin', '2021-08-31 11:01:00', 'admin', '2021-08-31 11:01:06', NULL);
INSERT INTO `sys_dict_data` VALUES (169, 0, '否', '0', 'game_true_or_false', NULL, 'default', 'N', '0', 'admin', '2021-08-31 11:03:08', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (170, 1, '是', '1', 'game_true_or_false', NULL, 'default', 'N', '0', 'admin', '2021-08-31 11:03:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (171, 0, '背包发货(需玩家离线)', '0', 'prop_sendtype', NULL, 'default', 'N', '0', 'admin', '2021-08-31 11:48:34', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (172, 1, '在线发货(妙手领取)', '1', 'prop_sendtype', NULL, 'default', 'N', '0', 'admin', '2021-08-31 11:48:47', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (173, 0, '指定设备', '0', 'reset_reg_number_type', NULL, 'default', 'N', '0', 'admin', '2021-09-01 14:39:43', 'admin', '2021-09-01 14:40:06', NULL);
INSERT INTO `sys_dict_data` VALUES (174, 1, '全部玩家', '1', 'reset_reg_number_type', NULL, 'default', 'N', '0', 'admin', '2021-09-01 14:39:59', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (175, 0, '离线', '0', 'role_inline', NULL, 'default', 'N', '0', 'admin', '2021-09-02 12:42:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (176, 1, '在线', '1', 'role_inline', NULL, 'default', 'N', '0', 'admin', '2021-09-02 12:42:24', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (177, 1, '大飞账号', '1', 'game_account_reg_type', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:00:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (178, 0, '普通账号', '0', 'game_account_reg_type', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:00:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (179, 0, '旧角色', '0', 'role_new_or_old', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:04:12', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (180, 1, '新角色', '1', 'role_new_or_old', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:04:24', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (181, 0, '仙', '0', 'role_x_m', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:07:21', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (182, 1, '魔', '1', 'role_x_m', NULL, 'default', 'N', '0', 'admin', '2021-09-02 14:07:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (183, 3, '定时公告', 'NOTICE', 'sys_job_group', NULL, 'default', 'N', '0', 'admin', '2021-09-02 18:02:29', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (184, 1, '开启', '1', 'game_open_status', NULL, 'default', 'N', '0', 'admin', '2021-09-03 09:51:52', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (185, 0, '关闭', '0', 'game_open_status', NULL, 'default', 'N', '0', 'admin', '2021-09-03 09:51:58', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (186, 0, '踢出玩家', '0', 'xinfa_do_type', NULL, 'default', 'N', '0', 'admin', '2021-09-03 13:42:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (187, 1, '封禁并踢出', '1', 'xinfa_do_type', NULL, 'default', 'N', '0', 'admin', '2021-09-03 13:42:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (188, 2, '踢出并删除账号', '2', 'xinfa_do_type', NULL, 'default', 'N', '0', 'admin', '2021-09-03 13:42:42', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (189, 0, '清理背包', '0', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-09-03 15:01:22', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (193, 0, '普通道具', '0', 'recall_type', NULL, 'default', 'N', '0', 'admin', '2021-09-04 12:34:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (194, 1, '叠加道具', '1', 'recall_type', NULL, 'default', 'N', '1', 'admin', '2021-09-04 12:34:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (195, 2, '宠物', '2', 'recall_type', NULL, 'default', 'N', '0', 'admin', '2021-09-04 12:34:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (196, 1, '回收成功', '1', 'recall_status', NULL, 'default', 'N', '0', 'admin', '2021-09-04 16:27:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (197, 0, '回收失败', '0', 'recall_status', NULL, 'default', 'N', '0', 'admin', '2021-09-04 16:27:40', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (198, 2, '使用背包', '2', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-09-13 22:25:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (206, 10, '查询我的充值积分', '10', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-09-19 01:38:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (208, 0, '不积累积分', '0', 'jfgz', NULL, 'default', 'N', '0', 'admin', '2021-09-20 09:20:44', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (209, 1, '金元宝积累积分', '1', 'jfgz', NULL, 'default', 'N', '0', 'admin', '2021-09-20 09:20:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (210, 2, '银元宝积累积分', '2', 'jfgz', NULL, 'default', 'N', '0', 'admin', '2021-09-20 09:21:19', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (211, 3, '金银全部积累', '3', 'jfgz', NULL, 'default', 'N', '0', 'admin', '2021-09-20 09:21:50', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (212, 12, '快捷抽奖入口', '12', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-09-21 19:44:10', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (213, 0, '不限制', '0', 'reg_role_type', NULL, 'default', 'N', '0', 'admin', '2021-09-23 14:33:08', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (214, 1, '仅旧角色', '1', 'reg_role_type', NULL, 'default', 'N', '0', 'admin', '2021-09-23 14:33:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (215, 2, '仅新角色', '2', 'reg_role_type', NULL, 'default', 'N', '0', 'admin', '2021-09-23 14:33:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (217, 0, '法宝共生', '0', 'activieDoType', NULL, 'default', 'N', '0', 'admin', '2021-09-26 15:36:58', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (218, 1, '首饰转换', '1', 'activieDoType', NULL, 'default', 'N', '0', 'admin', '2021-09-26 16:22:55', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (219, 2, '白帮忙', '2', 'activieDoType', NULL, 'default', 'N', '0', 'admin', '2021-09-26 16:44:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (220, 3, '门派转换', '3', 'activieDoType', NULL, 'default', 'N', '0', 'admin', '2021-09-26 16:55:45', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (221, 4, '新旧转换', '4', 'activieDoType', NULL, 'default', 'N', '0', 'admin', '2021-09-26 16:55:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (222, 0, '小额抽奖', '0', 'luck_do_type', NULL, 'default', 'N', '0', 'admin', '2021-05-24 13:33:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (223, 1, '大额抽奖', '1', 'luck_do_type', NULL, 'default', 'N', '0', 'admin', '2021-05-24 13:33:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (224, 1, 'CDK兑换', '1', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-09-30 07:00:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (225, 0, '个人平台', '0', 'order_channel', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:21:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (226, 1, '预留平台1', '1', 'order_channel', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:22:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (227, 2, '预留平台2', '2', 'order_channel', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:22:07', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (228, 3, '预留平台3', '3', 'order_channel', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:22:16', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (232, 0, '支付宝', 'alipay', 'pay_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:24:17', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (233, 1, '微信', 'wxpay', 'pay_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:24:30', 'admin', '2021-10-19 10:27:36', NULL);
INSERT INTO `sys_dict_data` VALUES (234, 0, '金元宝充值', '0', 'order_do_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:25:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (235, 1, '银元宝充值', '1', 'order_do_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:25:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (236, 2, '道具兑换', '2', 'order_do_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:25:18', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (237, 0, '系统回调', '0', 'order_update_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:26:01', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (238, 1, '手动补单', '1', 'order_update_type', NULL, 'default', 'N', '0', 'admin', '2021-10-18 12:26:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (239, 0, '待支付', '0', 'order_status', NULL, 'default', 'N', '0', 'admin', '2021-10-18 16:59:35', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (240, 1, '已过期', '1', 'order_status', NULL, 'default', 'N', '0', 'admin', '2021-10-18 16:59:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (241, 2, '已支付', '2', 'order_status', NULL, 'default', 'N', '0', 'admin', '2021-10-18 16:59:54', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (242, 3, '支付失败', '3', 'order_status', NULL, 'default', 'N', '0', 'admin', '2021-10-18 17:46:46', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (243, 13, '充值引导', '13', 'helper_dict', NULL, 'default', 'N', '0', 'admin', '2021-10-19 09:55:37', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (244, 3, '引路人发货(直接到背包)', '2', 'prop_sendtype', NULL, 'default', 'N', '0', 'admin', '2021-11-14 16:13:00', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (245, 0, '角色转移', '0', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 09:39:09', 'admin', '2021-12-13 10:55:32', NULL);
INSERT INTO `sys_dict_data` VALUES (246, 1, '换绑IP', '1', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:13:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (247, 2, '踢出全部玩家', '2', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:13:53', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (248, 3, '发货-离线背包', '3', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:14:07', 'admin', '2021-12-13 10:28:15', NULL);
INSERT INTO `sys_dict_data` VALUES (249, 4, '妙手发货', '4', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:28:30', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (250, 5, '发货-引路人空投', '5', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:28:38', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (251, 6, '积分操作', '6', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:38:33', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (252, 7, '元宝操作', '7', 'sys_oper_type', NULL, 'default', 'N', '0', 'admin', '2021-12-13 10:38:43', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (253, 8, '系统错误', '8', 'sys_oper_type', NULL, 'warning', 'N', '0', 'admin', '2021-12-13 10:55:05', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
                                  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
                                  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
                                  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
                                  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
                                  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                                  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                                  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                                  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                                  PRIMARY KEY (`dict_id`) USING BTREE,
                                  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 137 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-08-22 18:51:29', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (100, 'cdk使用状态', 'game_cdk_status', '0', 'admin', '2021-08-25 11:16:31', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (101, 'CDK类型', 'game_cdk_type', '0', 'admin', '2021-08-25 11:17:27', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (102, 'CDK使用类型', 'cdk_use_type', '0', 'admin', '2021-08-25 13:50:18', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (103, '道具绑定类型', 'game_prop_bind_type', '0', 'admin', '2021-08-25 15:40:32', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (104, '线路状态', 'game_server_line_status', '0', 'admin', '2021-08-26 11:08:55', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (105, '角色权限', 'role_pri_type', '0', 'admin', '2021-08-27 10:22:38', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (107, '玩家日志操作类型', 'player_log_do_type', '0', 'admin', '2021-08-27 10:50:24', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (108, '元宝消费类型', 'game_cost_platform', '0', 'admin', '2021-08-27 11:30:52', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (109, 'GM操作类型', 'gm_do_type', '0', 'admin', '2021-08-27 12:09:28', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (110, '游戏妙手回收类型', 'game_recall_miaoshou_recal_type', '0', 'admin', '2021-08-28 14:51:30', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (111, '游戏妙手回收状态', 'game_recall_miaoshou_finish_status', '0', 'admin', '2021-08-28 14:52:12', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (112, '玩家角色门派', 'game_polar', '0', 'admin', '2021-07-28 00:11:52', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (113, '玩家性别', 'game_sex', '0', 'admin', '2021-07-28 00:16:07', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (114, '游戏消息类型', 'game_message_type', '0', 'admin', '2021-07-29 15:35:59', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (115, '游戏是否', 'game_true_or_false', '0', 'admin', '2021-08-31 11:02:27', 'admin', '2021-08-31 11:03:49', '0 否 1 是 公用');
INSERT INTO `sys_dict_type` VALUES (117, '发货类型', 'prop_sendtype', '0', 'admin', '2021-08-31 11:48:09', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (118, '重置最大注册数量类型', 'reset_reg_number_type', '0', 'admin', '2021-09-01 14:39:28', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (119, '角色是否在线', 'role_inline', '0', 'admin', '2021-09-02 12:42:02', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (120, '账号注册类型', 'game_account_reg_type', '0', 'admin', '2021-09-02 14:00:15', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (121, '角色新旧', 'role_new_or_old', '0', 'admin', '2021-09-02 14:03:57', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (122, '角色仙魔', 'role_x_m', '0', 'admin', '2021-09-02 14:07:00', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (123, '游戏字典是否开启', 'game_open_status', '0', 'admin', '2021-09-03 09:51:35', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (124, '心法防御惩罚类型', 'xinfa_do_type', '0', 'admin', '2021-09-03 13:41:45', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (125, '小助手功能字典', 'helper_dict', '0', 'admin', '2021-09-03 15:01:04', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (126, '回收类型', 'recall_type', '0', 'admin', '2021-09-04 12:34:21', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (127, '回收状态', 'recall_status', '0', 'admin', '2021-09-04 16:27:21', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (128, '积分积累规则', 'jfgz', '0', 'admin', '2021-09-20 09:20:29', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (129, '注册角色新旧类型', 'reg_role_type', '0', 'admin', '2021-09-23 14:32:55', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (130, '活动操作类型', 'activieDoType', '0', 'admin', '2021-09-26 15:36:43', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (131, '抽奖类型', 'luck_do_type', '0', 'admin', '2021-05-24 13:32:30', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (132, '订单通道', 'order_channel', '0', 'admin', '2021-10-18 12:21:24', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (133, '订单状态', 'order_status', '0', 'admin', '2021-10-18 12:22:35', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (134, '付款方式', 'pay_type', '0', 'admin', '2021-10-18 12:24:03', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (135, '订单业务类型', 'order_do_type', '0', 'admin', '2021-10-18 12:24:52', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (136, '订单更新方式', 'order_update_type', '0', 'admin', '2021-10-18 12:25:39', '', NULL, NULL);

-- ----------------------------
-- Table structure for GAME_ORDER
-- ----------------------------
DROP TABLE IF EXISTS `GAME_ORDER`;
CREATE TABLE `GAME_ORDER`  (
                               `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
                               `account` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '用户账号',
                               `order_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '订单号',
                               `channel` int(1) NOT NULL COMMENT '订单付款通道',
                               `price` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '订单金额',
                               `pay_status` int(1) NOT NULL COMMENT '订单状态',
                               `pay_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT '付款方式',
                               `channel_order_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '第三方订单号',
                               `pay_time` datetime(0) NULL DEFAULT NULL COMMENT '支付时间',
                               `do_type` int(1) NOT NULL COMMENT '业务类型',
                               `do_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '业务数据',
                               `complete_type` int(1) NULL DEFAULT NULL COMMENT '订单更新方式',
                               `create_time` datetime(0) NOT NULL COMMENT '创建时间',
                               `dec_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单备注',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
                            `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
                            `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
                            `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
                            `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
                            `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
                            `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
                            `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
                            `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
                            `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                            `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                            `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                            `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
                            PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
-- INSERT INTO `sys_job` VALUES (6, '在线泡点', 'DEFAULT', 'ryTask.sendPopGif()', '0 0/3 * * * ?', '0', '1', '1', '', '2021-09-03 10:37:39', '', '2021-09-06 14:02:59', '');
INSERT INTO `sys_job` VALUES (7, '定时公告-每隔30分钟发一次', 'NOTICE', 'ryTask.sendNotification(120,\'#R欢迎各位玩家在本服愉快的玩耍，如需充值请联系群主！\')', '0 0/30 * * * ? ', '3', '1', '1', 'admin', '2021-09-03 12:28:12', 'admin', '2021-09-06 14:02:18', '');
INSERT INTO `sys_job` VALUES (8, '宠物回收', 'SYSTEM', 'ryTask.petRecall()', '0/20 * * * * ?', '3', '1', '0', 'admin', '2021-09-04 15:44:03', 'admin', '2021-09-30 11:34:07', '');
INSERT INTO `sys_job` VALUES (9, '每天凌晨清理数据', 'SYSTEM', 'ryTask.everyDayClear()', '0 0 0 * * ? ', '3', '1', '0', 'admin', '2021-09-05 17:16:22', '', '2021-09-05 17:16:27', '');
INSERT INTO `sys_job` VALUES (10, '数据库备份', 'SYSTEM', 'ryTask.backPackDB()', '0 0 2 * * ? ', '3', '1', '0', 'admin', '2021-09-07 17:31:54', '', '2021-09-07 17:32:00', '');
INSERT INTO `sys_job` VALUES (11, '刷新充值积分排行榜', 'SYSTEM', 'ryTask.refreshTop()', '0/5 * * * * ?', '1', '1', '1', 'admin', '2021-09-21 08:11:35', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
                                `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
                                `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
                                `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
                                `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
                                `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
                                `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
                                `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
                                `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                                PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8198 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
                                   `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
                                   `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
                                   `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
                                   `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
                                   `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
                                   `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
                                   `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
                                   `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
                                   `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
                                   PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------


-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
                             `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
                             `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
                             `order_num` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
                             `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
                             `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
                             `is_frame` int(11) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
                             `is_cache` int(11) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
                             `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
                             `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
                             `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
                             `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
                             PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2295 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '1', '0', '', 'system', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:03:25', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '1', '0', '', 'monitor', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:04:38', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '1', '0', '', 'tool', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:04:52', '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-06 22:09:01', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '1', '1', 'system:role:list', 'peoples', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:30:18', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 2228, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:03:06', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '1', '1', 'system:dept:list', 'tree', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:30:27', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '1', '1', 'system:post:list', 'post', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:30:31', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 2228, 4, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:57:05', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '1', '1', 'system:config:list', 'edit', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:30:41', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '1', '0', 'system:notice:list', 'message', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:28:11', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '1', '0', '', 'log', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-06 09:21:45', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '1', '1', 'monitor:online:list', 'online', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-22 19:30:04', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2228, 200, 'job', 'monitor/job/index', 1, 0, 'C', '1', '0', 'monitor:job:list', 'job', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:56:49', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '1', '0', 'monitor:druid:list', 'druid', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:04:31', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '1', '0', 'monitor:server:list', 'server', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-05 21:36:32', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '1', '0', 'monitor:cache:list', 'redis', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-05 21:36:38', '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '1', '1', 'tool:build:list', 'build', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-25 09:50:43', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 2228, 199, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-26 10:56:56', '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '1', '1', 'tool:swagger:list', 'swagger', 'admin', '2021-08-22 18:51:29', 'admin', '2021-08-25 09:50:38', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 2055, 80, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2021-08-22 18:51:29', 'admin', '2021-12-13 09:19:41', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 2055, 90, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2021-08-22 18:51:29', 'admin', '2021-09-06 09:22:37', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2021-08-22 18:51:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '玩家管理【皮】', 0, 600, 'player', NULL, 1, 0, 'M', '0', '0', '', 'user', 'admin', '2021-08-22 18:55:24', 'admin', '2021-10-21 14:33:42', '');
INSERT INTO `sys_menu` VALUES (2004, '通知管理【蛋】', 0, 601, 'notice', NULL, 1, 0, 'M', '0', '0', '', 'email', 'admin', '2021-08-22 19:00:05', 'admin', '2021-10-21 14:33:49', '');
INSERT INTO `sys_menu` VALUES (2005, '定时公告', 2004, 1, 'notice', 'notice/notice/index', 1, 0, 'C', '0', '0', '', 'time', 'admin', '2021-08-22 19:00:53', 'admin', '2021-05-24 14:43:33', '');
INSERT INTO `sys_menu` VALUES (2007, '登陆公告', 2004, 3, 'login', 'notice/login/index', 1, 0, 'C', '0', '0', '', 'logininfor', 'admin', '2021-08-22 19:01:56', 'admin', '2021-05-24 14:43:43', '');
INSERT INTO `sys_menu` VALUES (2008, '登陆邮件', 2004, 4, 'email', 'notice/email/index', 1, 0, 'C', '0', '0', '', 'component', 'admin', '2021-08-22 19:02:43', 'admin', '2021-05-24 14:43:53', '');
INSERT INTO `sys_menu` VALUES (2009, '游戏服务【祝】', 0, 602, 'gameservice', NULL, 1, 0, 'M', '0', '0', '', 'international', 'admin', '2021-08-22 19:07:32', 'admin', '2021-10-21 14:33:55', '');
INSERT INTO `sys_menu` VALUES (2016, '小助手', 0, 99999, 'config', 'helper/config/index', 1, 0, 'C', '0', '0', '', 'build', 'admin', '2021-08-22 19:13:29', 'admin', '2021-10-12 17:52:26', '');
INSERT INTO `sys_menu` VALUES (2023, 'CDK兑换活动', 2229, 80, 'cdk', NULL, 1, 0, 'M', '0', '0', '', 'education', 'admin', '2021-08-22 19:21:51', 'admin', '2021-09-26 11:33:14', '');
INSERT INTO `sys_menu` VALUES (2026, '宠物道具回收', 2229, 90, 'recall', NULL, 1, 0, 'M', '0', '0', '', 'swagger', 'admin', '2021-08-22 19:24:27', 'admin', '2021-10-05 15:51:23', '');
INSERT INTO `sys_menu` VALUES (2029, '在线泡点活动', 2229, 70, 'paodian', NULL, 1, 0, 'M', '0', '0', '', 'date-range', 'admin', '2021-08-22 20:07:57', 'admin', '2021-05-24 14:47:30', '');
INSERT INTO `sys_menu` VALUES (2030, '泡点配置', 2029, 1, 'config', 'paodian/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-08-22 20:08:21', 'admin', '2021-05-24 14:55:26', '');
INSERT INTO `sys_menu` VALUES (2034, '便捷开关', 2228, 1, 'tools', 'other/tools', 1, 0, 'C', '0', '0', '', 'tool', 'admin', '2021-08-22 20:12:03', 'admin', '2021-09-26 10:51:29', '');
INSERT INTO `sys_menu` VALUES (2036, '防御配置', 2224, 1, 'config', 'xinfa/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-08-22 20:17:44', 'admin', '2021-05-24 16:47:48', '');
INSERT INTO `sys_menu` VALUES (2038, '网关管理【位】', 0, 604, 'gateway', NULL, 1, 0, 'M', '0', '0', '', 'component', 'admin', '2021-08-22 20:59:42', 'admin', '2021-10-21 14:34:09', '');
INSERT INTO `sys_menu` VALUES (2039, '网关配置', 2038, 1, 'gateway', 'gateway/config/index', 1, 0, 'C', '0', '0', '', 'date', 'admin', '2021-08-22 20:59:57', 'admin', '2021-08-25 14:46:13', '');
INSERT INTO `sys_menu` VALUES (2043, '南极抽奖活动', 2229, 100, 'luck', NULL, 1, 0, 'M', '0', '0', '', 'row', 'admin', '2021-08-22 21:05:49', 'admin', '2021-05-24 14:47:41', '');
INSERT INTO `sys_menu` VALUES (2044, '抽奖配置', 2043, 1, 'config', 'luck/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-08-22 21:06:07', 'admin', '2021-05-24 14:55:51', '');
INSERT INTO `sys_menu` VALUES (2045, '抽奖记录', 2043, 2, 'log', 'luck/log/index', 1, 0, 'C', '0', '0', '', 'log', 'admin', '2021-08-22 21:06:48', 'admin', '2021-05-24 14:55:59', '');
INSERT INTO `sys_menu` VALUES (2046, '排行管理', 0, 14, 'top', NULL, 1, 0, 'M', '1', '0', '', 'component', 'admin', '2021-08-23 10:00:06', 'admin', '2021-09-03 09:37:56', '');
INSERT INTO `sys_menu` VALUES (2047, '道行排行榜', 2046, 1, 'tao', NULL, 1, 0, 'M', '0', '0', NULL, 'code', 'admin', '2021-08-23 10:03:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '宠物排行榜', 2046, 2, 'pet', NULL, 1, 0, 'M', '0', '0', NULL, 'button', 'admin', '2021-08-23 10:03:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '帮派排行榜', 2046, 3, 'bangpai', NULL, 1, 0, 'M', '0', '0', NULL, 'dashboard', 'admin', '2021-08-23 10:03:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '充值积分榜', 2046, 4, 'jifen', NULL, 1, 0, 'M', '0', '0', NULL, 'tree-table', 'admin', '2021-08-23 10:04:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '速度排行榜', 2046, 5, 'fast', NULL, 1, 0, 'M', '0', '0', NULL, 'download', 'admin', '2021-08-23 10:06:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '防御排行榜', 2046, 6, 'fangyu', NULL, 1, 0, 'M', '0', '0', NULL, 'example', 'admin', '2021-08-23 10:06:56', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '法伤排行榜', 2046, 7, 'fashang', NULL, 1, 0, 'M', '0', '0', '', 'education', 'admin', '2021-08-23 10:07:15', 'admin', '2021-08-23 10:07:51', '');
INSERT INTO `sys_menu` VALUES (2054, '物伤排行榜', 2046, 8, 'wushang', NULL, 1, 0, 'M', '0', '0', NULL, 'date', 'admin', '2021-08-23 10:07:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2055, '日志管理【板】', 0, 606, 'log', NULL, 1, 0, 'M', '0', '0', '', 'dict', 'admin', '2021-08-23 10:16:50', 'admin', '2021-10-21 14:34:25', '');
INSERT INTO `sys_menu` VALUES (2075, 'CDK管理', 2023, 1, 'CDK', 'cdk/CDK/index', 1, 0, 'C', '0', '0', 'cdk:CDK:list', 'swagger', 'admin', '2021-08-25 11:29:53', 'admin', '2021-05-24 14:55:39', 'CDK管理菜单');
INSERT INTO `sys_menu` VALUES (2076, 'CDK管理查询', 2075, 1, '#', '', 1, 0, 'F', '0', '0', 'cdk:CDK:query', '#', 'admin', '2021-08-25 11:29:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, 'CDK管理新增', 2075, 2, '#', '', 1, 0, 'F', '0', '0', 'cdk:CDK:add', '#', 'admin', '2021-08-25 11:29:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2078, 'CDK管理修改', 2075, 3, '#', '', 1, 0, 'F', '0', '0', 'cdk:CDK:edit', '#', 'admin', '2021-08-25 11:29:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, 'CDK管理删除', 2075, 4, '#', '', 1, 0, 'F', '0', '0', 'cdk:CDK:remove', '#', 'admin', '2021-08-25 11:29:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2080, 'CDK管理导出', 2075, 5, '#', '', 1, 0, 'F', '0', '0', 'cdk:CDK:export', '#', 'admin', '2021-08-25 11:29:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2087, '兑换记录', 2023, 2, 'log', 'cdk/log/index', 1, 0, 'C', '0', '0', 'cdk:log:list', 'log', 'admin', '2021-08-25 13:57:50', 'admin', '2021-09-26 11:06:40', 'CDK使用记录菜单');
INSERT INTO `sys_menu` VALUES (2088, 'CDK使用记录查询', 2087, 1, '#', '', 1, 0, 'F', '0', '0', 'cdk:log:query', '#', 'admin', '2021-08-25 13:57:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2089, 'CDK使用记录新增', 2087, 2, '#', '', 1, 0, 'F', '0', '0', 'cdk:log:add', '#', 'admin', '2021-08-25 13:57:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2090, 'CDK使用记录修改', 2087, 3, '#', '', 1, 0, 'F', '0', '0', 'cdk:log:edit', '#', 'admin', '2021-08-25 13:57:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2091, 'CDK使用记录删除', 2087, 4, '#', '', 1, 0, 'F', '0', '0', 'cdk:log:remove', '#', 'admin', '2021-08-25 13:57:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2092, 'CDK使用记录导出', 2087, 5, '#', '', 1, 0, 'F', '0', '0', 'cdk:log:export', '#', 'admin', '2021-08-25 13:57:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2093, 'AAA服务', 2009, 2, 'aaa', 'gameservice/aaa/index', 1, 0, 'C', '0', '0', 'gameservice:aaa:list', 'server', 'admin', '2021-08-26 10:26:27', 'admin', '2021-08-26 10:31:15', 'AAA·þÎñ²Ëµ¥');
INSERT INTO `sys_menu` VALUES (2094, 'AAA服务查询', 2093, 1, '#', '', 1, 0, 'F', '0', '0', 'gameservice:aaa:query', '#', 'admin', '2021-08-26 10:26:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2095, 'AAA服务新增', 2093, 2, '#', '', 1, 0, 'F', '0', '0', 'gameservice:aaa:add', '#', 'admin', '2021-08-26 10:26:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2096, 'AAA服务修改', 2093, 3, '#', '', 1, 0, 'F', '0', '0', 'gameservice:aaa:edit', '#', 'admin', '2021-08-26 10:26:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2097, 'AAA服务删除', 2093, 4, '#', '', 1, 0, 'F', '0', '0', 'gameservice:aaa:remove', '#', 'admin', '2021-08-26 10:26:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2098, 'AAA服务导出', 2093, 5, '#', '', 1, 0, 'F', '0', '0', 'gameservice:aaa:export', '#', 'admin', '2021-08-26 10:26:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2099, 'DBA服务', 2009, 3, 'dba', 'gameservice/dba/index', 1, 0, 'C', '0', '0', 'gameservice:dba:list', 'server', 'admin', '2021-08-26 10:45:21', 'admin', '2021-08-26 10:48:55', 'DBA·þÎñ²Ëµ¥');
INSERT INTO `sys_menu` VALUES (2100, 'DBA服务查询', 2099, 1, '#', '', 1, 0, 'F', '0', '0', 'gameservice:dba:query', '#', 'admin', '2021-08-26 10:45:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2101, 'DBA服务新增', 2099, 2, '#', '', 1, 0, 'F', '0', '0', 'gameservice:dba:add', '#', 'admin', '2021-08-26 10:45:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2102, 'DBA服务修改', 2099, 3, '#', '', 1, 0, 'F', '0', '0', 'gameservice:dba:edit', '#', 'admin', '2021-08-26 10:45:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2103, 'DBA服务删除', 2099, 4, '#', '', 1, 0, 'F', '0', '0', 'gameservice:dba:remove', '#', 'admin', '2021-08-26 10:45:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2104, 'DBA服务导出', 2099, 5, '#', '', 1, 0, 'F', '0', '0', 'gameservice:dba:export', '#', 'admin', '2021-08-26 10:45:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2105, 'CCS服务', 2009, 4, 'ccs', 'gameservice/ccs/index', 1, 0, 'C', '0', '0', 'gameservice:ccs:list', 'server', 'admin', '2021-08-26 11:19:00', 'admin', '2021-08-26 11:26:44', 'CCS·þÎñ²Ëµ¥');
INSERT INTO `sys_menu` VALUES (2106, 'CCS服务查询', 2105, 1, '#', '', 1, 0, 'F', '0', '0', 'gameservice:ccs:query', '#', 'admin', '2021-08-26 11:19:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2107, 'CCS服务新增', 2105, 2, '#', '', 1, 0, 'F', '0', '0', 'gameservice:ccs:add', '#', 'admin', '2021-08-26 11:19:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2108, 'CCS服务修改', 2105, 3, '#', '', 1, 0, 'F', '0', '0', 'gameservice:ccs:edit', '#', 'admin', '2021-08-26 11:19:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2109, 'CCS服务删除', 2105, 4, '#', '', 1, 0, 'F', '0', '0', 'gameservice:ccs:remove', '#', 'admin', '2021-08-26 11:19:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2110, 'CCS服务导出', 2105, 5, '#', '', 1, 0, 'F', '0', '0', 'gameservice:ccs:export', '#', 'admin', '2021-08-26 11:19:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2111, 'CSA服务', 2009, 5, 'csa', 'gameservice/csa/index', 1, 0, 'C', '0', '0', 'gameservice:csa:list', 'server', 'admin', '2021-08-26 11:19:12', 'admin', '2021-08-26 11:26:34', 'CSA·þÎñ²Ëµ¥');
INSERT INTO `sys_menu` VALUES (2112, 'CSA服务查询', 2111, 1, '#', '', 1, 0, 'F', '0', '0', 'gameservice:csa:query', '#', 'admin', '2021-08-26 11:19:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2113, 'CSA服务新增', 2111, 2, '#', '', 1, 0, 'F', '0', '0', 'gameservice:csa:add', '#', 'admin', '2021-08-26 11:19:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2114, 'CSA服务修改', 2111, 3, '#', '', 1, 0, 'F', '0', '0', 'gameservice:csa:edit', '#', 'admin', '2021-08-26 11:19:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2115, 'CSA服务删除', 2111, 4, '#', '', 1, 0, 'F', '0', '0', 'gameservice:csa:remove', '#', 'admin', '2021-08-26 11:19:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2116, 'CSA服务导出', 2111, 5, '#', '', 1, 0, 'F', '0', '0', 'gameservice:csa:export', '#', 'admin', '2021-08-26 11:19:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2117, '线路服务', 2009, 1, 'line', 'gameservice/line/index', 1, 0, 'C', '0', '0', 'gameservice:line:list', 'server', 'admin', '2021-08-26 11:20:31', 'admin', '2021-08-26 11:26:59', 'ÓÎÏ·ÏßÂ·²Ëµ¥');
INSERT INTO `sys_menu` VALUES (2118, '线路服务查询', 2117, 1, '#', '', 1, 0, 'F', '0', '0', 'gameservice:line:query', '#', 'admin', '2021-08-26 11:20:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2119, '线路服务新增', 2117, 2, '#', '', 1, 0, 'F', '0', '0', 'gameservice:line:add', '#', 'admin', '2021-08-26 11:20:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2120, '线路服务修改', 2117, 3, '#', '', 1, 0, 'F', '0', '0', 'gameservice:line:edit', '#', 'admin', '2021-08-26 11:20:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2121, '线路服务删除', 2117, 4, '#', '', 1, 0, 'F', '0', '0', 'gameservice:line:remove', '#', 'admin', '2021-08-26 11:20:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2122, '线路服务导出', 2117, 5, '#', '', 1, 0, 'F', '0', '0', 'gameservice:line:export', '#', 'admin', '2021-08-26 11:20:31', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2123, '玩家管理', 2000, 1, 'account', 'player/account/index', 1, 0, 'C', '0', '0', 'player:account:list', 'people', 'admin', '2021-08-27 10:26:20', 'admin', '2021-05-24 14:45:11', '玩家账号菜单');
INSERT INTO `sys_menu` VALUES (2124, '玩家账号查询', 2123, 1, '#', '', 1, 0, 'F', '0', '0', 'player:account:query', '#', 'admin', '2021-08-27 10:26:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2125, '玩家账号新增', 2123, 2, '#', '', 1, 0, 'F', '0', '0', 'player:account:add', '#', 'admin', '2021-08-27 10:26:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2126, '玩家账号修改', 2123, 3, '#', '', 1, 0, 'F', '0', '0', 'player:account:edit', '#', 'admin', '2021-08-27 10:26:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2127, '玩家账号删除', 2123, 4, '#', '', 1, 0, 'F', '0', '0', 'player:account:remove', '#', 'admin', '2021-08-27 10:26:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2128, '玩家账号导出', 2123, 5, '#', '', 1, 0, 'F', '0', '0', 'player:account:export', '#', 'admin', '2021-08-27 10:26:20', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2129, '角色日志', 2055, 2, 'userlog', 'gamelogs/userlog/index', 1, 0, 'C', '0', '0', 'gamelogs:userlog:list', 'log', 'admin', '2021-08-27 10:57:35', 'admin', '2021-08-27 11:41:05', '角色日志菜单');
INSERT INTO `sys_menu` VALUES (2130, '角色日志查询', 2129, 1, '#', '', 1, 0, 'F', '0', '0', 'player:log:query', '#', 'admin', '2021-08-27 10:57:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2131, '角色日志新增', 2129, 2, '#', '', 1, 0, 'F', '0', '0', 'player:log:add', '#', 'admin', '2021-08-27 10:57:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2132, '角色日志修改', 2129, 3, '#', '', 1, 0, 'F', '0', '0', 'player:log:edit', '#', 'admin', '2021-08-27 10:57:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2133, '角色日志删除', 2129, 4, '#', '', 1, 0, 'F', '0', '0', 'player:log:remove', '#', 'admin', '2021-08-27 10:57:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2134, '角色日志导出', 2129, 5, '#', '', 1, 0, 'F', '0', '0', 'player:log:export', '#', 'admin', '2021-08-27 10:57:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2135, '商城购买', 2055, 1, 'costlog', 'gamelogs/costlog/index', 1, 0, 'C', '0', '0', 'gamelogs:costlog:list', 'log', 'admin', '2021-08-27 11:33:49', 'admin', '2021-08-27 11:38:58', '商城购买菜单');
INSERT INTO `sys_menu` VALUES (2136, '商城购买查询', 2135, 1, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:costlog:query', '#', 'admin', '2021-08-27 11:33:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2137, '商城购买新增', 2135, 2, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:costlog:add', '#', 'admin', '2021-08-27 11:33:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2138, '商城购买修改', 2135, 3, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:costlog:edit', '#', 'admin', '2021-08-27 11:33:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2139, '商城购买删除', 2135, 4, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:costlog:remove', '#', 'admin', '2021-08-27 11:33:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2140, '商城购买导出', 2135, 5, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:costlog:export', '#', 'admin', '2021-08-27 11:33:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2153, 'GM日志', 2055, 3, 'gmlog', 'gamelogs/gmlog/index', 1, 0, 'C', '0', '0', 'gamelogs:gmlog:list', 'log', 'admin', '2021-08-27 12:12:15', 'admin', '2021-08-27 12:21:17', 'GM日志菜单');
INSERT INTO `sys_menu` VALUES (2154, 'GM日志查询', 2153, 1, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:gmlog:query', '#', 'admin', '2021-08-27 12:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2155, 'GM日志新增', 2153, 2, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:gmlog:add', '#', 'admin', '2021-08-27 12:12:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2156, 'GM日志修改', 2153, 3, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:gmlog:edit', '#', 'admin', '2021-08-27 12:12:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2157, 'GM日志删除', 2153, 4, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:gmlog:remove', '#', 'admin', '2021-08-27 12:12:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2158, 'GM日志导出', 2153, 5, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:gmlog:export', '#', 'admin', '2021-08-27 12:12:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2165, '妙手回收', 2026, 1, 'miaoshou', 'recall/miaoshou/index', 1, 0, 'C', '1', '0', 'recall:miaoshou:list', 'job', 'admin', '2021-08-28 14:58:35', 'admin', '2021-09-04 16:58:02', '妙手回收菜单');
INSERT INTO `sys_menu` VALUES (2166, '妙手回收查询', 2165, 1, '#', '', 1, 0, 'F', '0', '0', 'recall:miaoshou:query', '#', 'admin', '2021-08-28 14:58:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2167, '妙手回收新增', 2165, 2, '#', '', 1, 0, 'F', '0', '0', 'recall:miaoshou:add', '#', 'admin', '2021-08-28 14:58:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2168, '妙手回收修改', 2165, 3, '#', '', 1, 0, 'F', '0', '0', 'recall:miaoshou:edit', '#', 'admin', '2021-08-28 14:58:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2169, '妙手回收删除', 2165, 4, '#', '', 1, 0, 'F', '0', '0', 'recall:miaoshou:remove', '#', 'admin', '2021-08-28 14:58:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2170, '妙手回收导出', 2165, 5, '#', '', 1, 0, 'F', '0', '0', 'recall:miaoshou:export', '#', 'admin', '2021-08-28 14:58:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2171, '角色管理', 0, 5, 'role', NULL, 1, 0, 'M', '1', '0', '', 'job', 'admin', '2021-08-29 09:32:58', 'admin', '2021-09-02 12:51:57', '');
INSERT INTO `sys_menu` VALUES (2172, '基础资料', 2171, 2, 'charInfo', 'role/charInfo/index', 1, 0, 'C', '0', '0', 'role:charInfo:list', 'button', 'admin', '2021-08-29 09:35:48', 'admin', '2021-08-31 17:52:33', '角色资料菜单');
INSERT INTO `sys_menu` VALUES (2173, '角色资料查询', 2172, 1, '#', '', 1, 0, 'F', '0', '0', 'role:charInfo:query', '#', 'admin', '2021-08-29 09:35:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2174, '角色资料新增', 2172, 2, '#', '', 1, 0, 'F', '0', '0', 'role:charInfo:add', '#', 'admin', '2021-08-29 09:35:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2175, '角色资料修改', 2172, 3, '#', '', 1, 0, 'F', '0', '0', 'role:charInfo:edit', '#', 'admin', '2021-08-29 09:35:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2176, '角色资料删除', 2172, 4, '#', '', 1, 0, 'F', '0', '0', 'role:charInfo:remove', '#', 'admin', '2021-08-29 09:35:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2177, '角色资料导出', 2172, 5, '#', '', 1, 0, 'F', '0', '0', 'role:charInfo:export', '#', 'admin', '2021-08-29 09:35:48', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2178, '角色GID查看', 2171, 1, 'gidinfo', 'role/gidinfo/index', 1, 0, 'C', '1', '0', 'role:gidinfo:list', 'list', 'admin', '2021-08-29 09:46:40', 'admin', '2021-08-31 17:52:25', '角色Gid资料菜单');
INSERT INTO `sys_menu` VALUES (2179, '角色Gid资料查询', 2178, 1, '#', '', 1, 0, 'F', '0', '0', 'role:gidinfo:query', '#', 'admin', '2021-08-29 09:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2180, '角色Gid资料新增', 2178, 2, '#', '', 1, 0, 'F', '0', '0', 'role:gidinfo:add', '#', 'admin', '2021-08-29 09:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2181, '角色Gid资料修改', 2178, 3, '#', '', 1, 0, 'F', '0', '0', 'role:gidinfo:edit', '#', 'admin', '2021-08-29 09:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2182, '角色Gid资料删除', 2178, 4, '#', '', 1, 0, 'F', '0', '0', 'role:gidinfo:remove', '#', 'admin', '2021-08-29 09:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2183, '角色Gid资料导出', 2178, 5, '#', '', 1, 0, 'F', '0', '0', 'role:gidinfo:export', '#', 'admin', '2021-08-29 09:46:40', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2184, '详细数据', 2171, 3, 'data', 'role/data/index', 1, 0, 'C', '0', '0', 'role:data:list', 'code', 'admin', '2021-08-29 09:51:01', 'admin', '2021-08-31 17:52:38', '角色数据菜单');
INSERT INTO `sys_menu` VALUES (2185, '角色数据查询', 2184, 1, '#', '', 1, 0, 'F', '0', '0', 'role:data:query', '#', 'admin', '2021-08-29 09:51:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2186, '角色数据新增', 2184, 2, '#', '', 1, 0, 'F', '0', '0', 'role:data:add', '#', 'admin', '2021-08-29 09:51:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2187, '角色数据修改', 2184, 3, '#', '', 1, 0, 'F', '0', '0', 'role:data:edit', '#', 'admin', '2021-08-29 09:51:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2188, '角色数据删除', 2184, 4, '#', '', 1, 0, 'F', '0', '0', 'role:data:remove', '#', 'admin', '2021-08-29 09:51:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2189, '角色数据导出', 2184, 5, '#', '', 1, 0, 'F', '0', '0', 'role:data:export', '#', 'admin', '2021-08-29 09:51:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2190, '注册记录', 2038, 2, 'log', 'gateway/log/index', 1, 0, 'C', '0', '0', 'gateway:log:list', 'log', 'admin', '2021-09-01 11:05:09', 'admin', '2021-09-01 11:28:48', '注册记录菜单');
INSERT INTO `sys_menu` VALUES (2191, '注册记录查询', 2190, 1, '#', '', 1, 0, 'F', '0', '0', 'gateway:log:query', '#', 'admin', '2021-09-01 11:05:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2192, '注册记录新增', 2190, 2, '#', '', 1, 0, 'F', '0', '0', 'gateway:log:add', '#', 'admin', '2021-09-01 11:05:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2193, '注册记录修改', 2190, 3, '#', '', 1, 0, 'F', '0', '0', 'gateway:log:edit', '#', 'admin', '2021-09-01 11:05:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2194, '注册记录删除', 2190, 4, '#', '', 1, 0, 'F', '0', '0', 'gateway:log:remove', '#', 'admin', '2021-09-01 11:05:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2195, '注册记录导出', 2190, 5, '#', '', 1, 0, 'F', '0', '0', 'gateway:log:export', '#', 'admin', '2021-09-01 11:05:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2196, '泡点记录', 2029, 2, 'log', 'paodian/log/index', 1, 0, 'C', '0', '0', 'paodian:log:list', 'log', 'admin', '2021-09-03 11:10:07', 'admin', '2021-09-03 11:11:49', '泡点记录菜单');
INSERT INTO `sys_menu` VALUES (2197, '泡点记录查询', 2196, 1, '#', '', 1, 0, 'F', '0', '0', 'paodian:log:query', '#', 'admin', '2021-09-03 11:10:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2198, '泡点记录新增', 2196, 2, '#', '', 1, 0, 'F', '0', '0', 'paodian:log:add', '#', 'admin', '2021-09-03 11:10:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2199, '泡点记录修改', 2196, 3, '#', '', 1, 0, 'F', '0', '0', 'paodian:log:edit', '#', 'admin', '2021-09-03 11:10:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2200, '泡点记录删除', 2196, 4, '#', '', 1, 0, 'F', '0', '0', 'paodian:log:remove', '#', 'admin', '2021-09-03 11:10:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2201, '泡点记录导出', 2196, 5, '#', '', 1, 0, 'F', '0', '0', 'paodian:log:export', '#', 'admin', '2021-09-03 11:10:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2202, '拦截日志', 2224, 2, 'log', 'xinfa/log/index', 1, 0, 'C', '0', '0', 'xinfa:log:list', 'log', 'admin', '2021-09-03 14:34:01', 'admin', '2021-05-24 16:47:57', '拦截日志菜单');
INSERT INTO `sys_menu` VALUES (2203, '拦截日志查询', 2202, 1, '#', '', 1, 0, 'F', '0', '0', 'xinfa:log:query', '#', 'admin', '2021-09-03 14:34:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2204, '拦截日志新增', 2202, 2, '#', '', 1, 0, 'F', '0', '0', 'xinfa:log:add', '#', 'admin', '2021-09-03 14:34:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2205, '拦截日志修改', 2202, 3, '#', '', 1, 0, 'F', '0', '0', 'xinfa:log:edit', '#', 'admin', '2021-09-03 14:34:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2206, '拦截日志删除', 2202, 4, '#', '', 1, 0, 'F', '0', '0', 'xinfa:log:remove', '#', 'admin', '2021-09-03 14:34:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2207, '拦截日志导出', 2202, 5, '#', '', 1, 0, 'F', '0', '0', 'xinfa:log:export', '#', 'admin', '2021-09-03 14:34:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2208, '回收配置', 2026, 0, 'recall', 'recall/config/index', 1, 0, 'C', '0', '0', NULL, 'swagger', 'admin', '2021-09-04 12:36:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2209, '回收记录', 2026, 1, 'log', 'recall/log/index', 1, 0, 'C', '0', '0', 'recall:log:list', 'log', 'admin', '2021-09-04 15:54:52', 'admin', '2021-09-04 16:58:32', '回收记录菜单');
INSERT INTO `sys_menu` VALUES (2210, '回收记录查询', 2209, 1, '#', '', 1, 0, 'F', '0', '0', 'recall:log:query', '#', 'admin', '2021-09-04 15:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2211, '回收记录新增', 2209, 2, '#', '', 1, 0, 'F', '0', '0', 'recall:log:add', '#', 'admin', '2021-09-04 15:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2212, '回收记录修改', 2209, 3, '#', '', 1, 0, 'F', '0', '0', 'recall:log:edit', '#', 'admin', '2021-09-04 15:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2213, '回收记录删除', 2209, 4, '#', '', 1, 0, 'F', '0', '0', 'recall:log:remove', '#', 'admin', '2021-09-04 15:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2214, '回收记录导出', 2209, 5, '#', '', 1, 0, 'F', '0', '0', 'recall:log:export', '#', 'admin', '2021-09-04 15:54:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2215, '领取记录', 2231, 2, 'level_up_log', 'gamelogs/level_up_log/index', 1, 0, 'C', '0', '0', 'gamelogs:level_up_log:list', 'log', 'admin', '2021-09-19 21:14:00', 'admin', '2021-09-26 11:36:01', '冲级奖励领取记录菜单');
INSERT INTO `sys_menu` VALUES (2216, '冲级奖励领取记录查询', 2215, 1, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:level_up_log:query', '#', 'admin', '2021-09-19 21:14:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2217, '冲级奖励领取记录新增', 2215, 2, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:level_up_log:add', '#', 'admin', '2021-09-19 21:14:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2218, '冲级奖励领取记录修改', 2215, 3, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:level_up_log:edit', '#', 'admin', '2021-09-19 21:14:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2219, '冲级奖励领取记录删除', 2215, 4, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:level_up_log:remove', '#', 'admin', '2021-09-19 21:14:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2220, '冲级奖励领取记录导出', 2215, 5, '#', '', 1, 0, 'F', '0', '0', 'gamelogs:level_up_log:export', '#', 'admin', '2021-09-19 21:14:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2221, '首饰防御', 2223, 2, 'shoushi', 'shoushi/config/index', 1, 0, 'C', '0', '0', '', 'button', 'admin', '2021-09-25 12:04:58', 'admin', '2021-10-05 15:48:53', '');
INSERT INTO `sys_menu` VALUES (2223, '防御系统【老】', 0, 605, 'safe', NULL, 1, 0, 'M', '0', '0', '', 'job', 'admin', '2021-09-26 09:37:15', 'admin', '2021-10-21 14:34:17', '');
INSERT INTO `sys_menu` VALUES (2224, '心法坐骑防御', 2223, 1, 'xinfa', NULL, 1, 0, 'M', '0', '0', '', 'button', 'admin', '2021-09-26 09:38:31', 'admin', '2021-09-26 09:47:43', '');
INSERT INTO `sys_menu` VALUES (2228, '系统配置【发】', 0, 607, 'pluginsystem', '', 1, 0, 'M', '0', '0', '', 'number', 'admin', '2021-09-26 10:02:21', 'admin', '2021-10-21 14:34:30', '');
INSERT INTO `sys_menu` VALUES (2229, '活动管理【各】', 0, 603, 'activity', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2021-09-26 10:08:22', 'admin', '2021-10-21 14:34:02', '');
INSERT INTO `sys_menu` VALUES (2230, '积分配置', 2228, 2, 'config', 'integral/config/index', 1, 0, 'C', '0', '0', '', 'edit', 'admin', '2021-09-26 10:56:36', 'admin', '2021-09-26 10:57:25', '');
INSERT INTO `sys_menu` VALUES (2231, '升级奖励活动', 2229, 120, 'levelup', NULL, 1, 0, 'M', '0', '0', '', 'component', 'admin', '2021-09-26 11:04:32', 'admin', '2021-09-26 11:33:51', '');
INSERT INTO `sys_menu` VALUES (2232, '奖品管理', 2231, 1, 'config', 'levelup/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:05:23', 'admin', '2021-05-24 14:56:20', '');
INSERT INTO `sys_menu` VALUES (2234, '法宝共生活动', 2229, 40, 'fabaogongsheng', '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2021-09-26 11:10:19', 'admin', '2021-09-26 11:32:24', '');
INSERT INTO `sys_menu` VALUES (2235, '功能配置', 2234, 1, 'config', 'fabaogongsheng/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:11:08', 'admin', '2021-05-24 14:54:50', '');
INSERT INTO `sys_menu` VALUES (2236, '共生记录', 2234, 2, 'log', 'fabaogongsheng/log/index', 1, 0, 'C', '0', '0', NULL, 'log', 'admin', '2021-09-26 11:11:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2237, '门派转换活动', 2229, 50, 'menpaizhuanhuan', NULL, 1, 0, 'M', '0', '0', '', 'button', 'admin', '2021-09-26 11:12:58', 'admin', '2021-09-26 11:32:27', '');
INSERT INTO `sys_menu` VALUES (2238, '转换配置', 2237, 1, 'config', 'menpaizhuanhuan/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:13:27', 'admin', '2021-05-24 14:55:01', '');
INSERT INTO `sys_menu` VALUES (2239, '转换记录', 2237, 2, 'log', 'menpaizhuanhuan/log/index', 1, 0, 'C', '0', '0', '', 'log', 'admin', '2021-09-26 11:13:49', 'admin', '2021-09-26 17:21:46', '');
INSERT INTO `sys_menu` VALUES (2240, '新旧转换活动', 2229, 60, 'xinjiuzhuanhuan', NULL, 1, 0, 'M', '0', '0', '', 'nested', 'admin', '2021-09-26 11:14:43', 'admin', '2021-05-24 14:46:56', '');
INSERT INTO `sys_menu` VALUES (2241, '转换配置', 2240, 1, 'config', 'xinjiuzhuanhuan/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:15:06', 'admin', '2021-05-24 14:55:14', '');
INSERT INTO `sys_menu` VALUES (2242, '转换记录', 2240, 2, 'log', 'xinjiuzhuanhuan/log/index', 1, 0, 'C', '0', '0', '', 'log', 'admin', '2021-09-26 11:15:34', 'admin', '2021-09-26 17:23:17', '');
INSERT INTO `sys_menu` VALUES (2243, '首饰次数重置', 2229, 30, 'shoushizhuanhuan', NULL, 1, 0, 'M', '0', '0', '', 'code', 'admin', '2021-09-26 11:16:19', 'admin', '2021-05-24 14:46:41', '');
INSERT INTO `sys_menu` VALUES (2244, '重置配置', 2243, 1, 'config', 'shoushizhuanhuan/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:17:22', 'admin', '2021-05-24 14:54:40', '');
INSERT INTO `sys_menu` VALUES (2245, '使用记录', 2243, 2, 'log', 'shoushizhuanhuan/log/index', 1, 0, 'C', '0', '0', '', 'log', 'admin', '2021-09-26 11:17:50', 'admin', '2021-09-26 16:24:47', '');
INSERT INTO `sys_menu` VALUES (2246, '白帮忙重置', 2229, 20, 'baibangmang', NULL, 1, 0, 'M', '0', '0', '', 'component', 'admin', '2021-09-26 11:19:10', 'admin', '2021-05-24 14:46:33', '');
INSERT INTO `sys_menu` VALUES (2247, '重置配置', 2246, 1, 'config', 'baibangmang/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:19:53', 'admin', '2021-05-24 14:54:29', '');
INSERT INTO `sys_menu` VALUES (2248, '重置记录', 2246, 2, 'log', 'baibangmang/log/index', 1, 0, 'C', '0', '0', NULL, 'log', 'admin', '2021-09-26 11:20:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2249, '活跃奖励活动', 2229, 110, 'active', NULL, 1, 0, 'M', '0', '0', '', 'skill', 'admin', '2021-09-26 11:20:51', 'admin', '2021-09-26 11:33:46', '');
INSERT INTO `sys_menu` VALUES (2250, '奖励配置', 2249, 1, 'config', 'active/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-09-26 11:21:21', 'admin', '2021-05-24 14:56:09', '');
INSERT INTO `sys_menu` VALUES (2251, '领取记录', 2249, 2, 'log', 'active/log/index', 1, 0, 'C', '0', '0', NULL, 'log', 'admin', '2021-09-26 11:21:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2261, 'ceshi', 2228, 111111, 'test', 'test/test', 1, 0, 'C', '0', '0', '', '#', 'admin', '2021-10-02 11:35:02', 'admin', '2021-10-02 20:42:01', '');
INSERT INTO `sys_menu` VALUES (2262, '商城防御', 2223, 3, 'shopsaf', 'shopsaf/config/index', 1, 0, 'C', '0', '0', '', 'button', 'admin', '2021-10-03 12:23:24', 'admin', '2021-10-05 15:49:12', '');
INSERT INTO `sys_menu` VALUES (2264, '封禁IP管理', 2223, 4, 'safe', 'ipsafe/config/index', 1, 0, 'C', '0', '0', NULL, 'cascader', 'admin', '2021-10-05 15:53:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2265, '假人系统【大】', 0, 608, 'jiaren', NULL, 1, 0, 'M', '0', '0', '', 'github', 'admin', '2021-10-07 10:49:50', 'admin', '2021-10-21 14:34:35', '');
INSERT INTO `sys_menu` VALUES (2266, '天庸城假人', 2265, 2, 'jiaren', 'jiaren/tianyongcheng/index', 1, 0, 'C', '0', '0', '', 'button', 'admin', '2021-10-07 10:50:30', 'admin', '2021-10-12 17:31:10', '');
INSERT INTO `sys_menu` VALUES (2267, '自动回复', 2265, 0, 'jiarenauto', 'jiaren/automsg/config', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-10-12 16:26:36', 'admin', '2021-10-12 17:30:59', '');
INSERT INTO `sys_menu` VALUES (2268, '自动喊话', 2265, 1, 'hanhua', 'jiaren/sendmsg/config', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-10-12 17:13:28', 'admin', '2021-10-12 17:31:06', '');
INSERT INTO `sys_menu` VALUES (2269, '支付系统【财】', 0, 609, 'pay', NULL, 1, 0, 'M', '0', '0', '', 'money', 'admin', '2021-10-18 10:23:38', 'admin', '2021-10-21 14:34:45', '');
INSERT INTO `sys_menu` VALUES (2271, '平台配置', 2269, 0, 'config', 'pay/config/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-10-18 10:24:29', 'admin', '2021-10-18 14:24:51', '');
INSERT INTO `sys_menu` VALUES (2284, '订单列表', 2269, 2, 'order', 'pay/order/index', 1, 0, 'C', '0', '0', 'pay:order:list', 'button', 'admin', '2021-10-18 12:35:46', 'admin', '2021-10-18 14:24:57', '订单列表菜单');
INSERT INTO `sys_menu` VALUES (2285, '订单列表查询', 2284, 1, '#', '', 1, 0, 'F', '0', '0', 'pay:order:query', '#', 'admin', '2021-10-18 12:35:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2286, '订单列表新增', 2284, 2, '#', '', 1, 0, 'F', '0', '0', 'pay:order:add', '#', 'admin', '2021-10-18 12:35:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2287, '订单列表修改', 2284, 3, '#', '', 1, 0, 'F', '0', '0', 'pay:order:edit', '#', 'admin', '2021-10-18 12:35:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2288, '订单列表删除', 2284, 4, '#', '', 1, 0, 'F', '0', '0', 'pay:order:remove', '#', 'admin', '2021-10-18 12:35:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2289, '订单列表导出', 2284, 5, '#', '', 1, 0, 'F', '0', '0', 'pay:order:export', '#', 'admin', '2021-10-18 12:35:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2290, '支付配置', 2269, 1, 'gconfig', 'pay/gconfig/index', 1, 0, 'C', '0', '0', '', 'swagger', 'admin', '2021-10-18 14:24:24', 'admin', '2021-10-18 14:25:01', '');
INSERT INTO `sys_menu` VALUES (2291, 'VIP奖励活动', 2229, 130, 'vip', NULL, 1, 0, 'M', '0', '0', NULL, 'date-range', 'admin', '2021-10-21 14:35:42', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2292, '活动配置', 2291, 0, 'config', 'vip/config/index', 1, 0, 'C', '0', '0', NULL, 'swagger', 'admin', '2021-10-21 14:36:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2294, '揽仙镇外假人', 2265, 3, 'lanxianzhenwai', 'jiaren/lanxianzhenwai/index', 1, 0, 'C', '0', '0', NULL, 'button', 'admin', '2021-10-23 10:04:41', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2295, '帮派管理', 2009, 5, 'data', 'party/data/index', 1, 0, 'C', '0', '0', '', 'button', 'admin', '2021-10-24 21:01:04', 'admin', '2021-10-24 21:01:45', '');
INSERT INTO `sys_menu` VALUES (2296, '特殊地图限制', 2229, 150, 'vipmap', 'vip/vipmap/index', 1, 0, 'C', '0', '0', '', 'button', 'admin', '2021-11-14 09:10:51', 'admin', '2021-12-02 12:16:18', '');
INSERT INTO `sys_menu` VALUES (2297, '刷道奖励活动', 2229, 140, 'taogift', NULL, 1, 0, 'M', '0', '0', NULL, 'bug', 'admin', '2021-11-20 07:16:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2298, '奖励配置', 2297, 1, 'taogift', 'taogift/config/index', 1, 0, 'C', '0', '0', NULL, 'button', 'admin', '2021-11-20 07:16:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2299, 'PK系统配置', 2229, 160, 'config', 'pk/config/index', 1, 0, 'C', '0', '0', NULL, 'button', 'admin', '2021-12-02 16:56:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2300, '刷塔限制活动', 2229, 170, 'resetST', 'resetST/index', 1, 0, 'C', '0', '0', NULL, 'bug', 'admin', '2021-12-11 09:39:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2301, '指定BOSS奖励系统', 2229, 180, 'choose', 'choosePK/index', 1, 0, 'C', '0', '0', NULL, 'button', 'admin', '2021-12-13 19:08:58', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
                               `notice_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
                               `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
                               `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
                               `notice_content` longblob NULL COMMENT '公告内容',
                               `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
                               `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                               `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                               `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                               `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                               PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
                                 `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
                                 `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
                                 `business_type` int(11) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
                                 `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
                                 `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
                                 `operator_type` int(11) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
                                 `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
                                 `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
                                 `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
                                 `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
                                 `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
                                 `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
                                 `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
                                 `status` int(11) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
                                 `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
                                 `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
                                 PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
                             `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
                             `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
                             `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
                             `post_sort` int(11) NOT NULL COMMENT '显示顺序',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
                             `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
                             `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
                             `role_sort` int(11) NOT NULL COMMENT '显示顺序',
                             `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
                             `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
                             `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
                                  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
                                  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                             `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
                             `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
                             `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
                             `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
                             `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
                             `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
                             `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
                             `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
                             `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
                             `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
                             `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
                             `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
                             `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
                             `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '1.60插件', '00', 'ry@163.com', '15888888888', '0', '', '$2a$10$CuSrqnV0NBeyaUFi0.elJ.jSxkonvBJKXnD8TkwioJ3ZnSFT/35pC', '0', '0', '127.0.0.1', '2021-09-29 10:05:42', 'admin', '2021-08-22 18:51:29', '', '2021-05-25 05:41:13', '管理员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
                                  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
                                  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
                                  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
                                  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------



use dl_adb_all;

-- 创建存储过程 检测必须字段
DROP PROCEDURE IF EXISTS PRO_ADD;
delimiter $$
CREATE PROCEDURE PRO_ADD()
BEGIN

    -- 检测aaa表中的proxy_port字段是否存在
    IF
NOT EXISTS (
                SELECT 1 FROM information_schema.`COLUMNS` WHERE TABLE_NAME='aaa' AND COLUMN_NAME='proxy_port')
    THEN

ALTER TABLE aaa
    ADD COLUMN proxy_port int(5) DEFAULT 0 COMMENT '代理端口' AFTER port;
UPDATE aaa
SET proxy_port = SOURE_BIND_PORT
WHERE aaa = 'aaa_daili';
END IF;


-- 检测server表中的proxy_port字段是否存在
    IF
NOT EXISTS (
                SELECT 1 FROM information_schema.`COLUMNS` WHERE TABLE_NAME='server' AND COLUMN_NAME='proxy_port')
    THEN

ALTER TABLE server
    ADD COLUMN proxy_port int(5) DEFAULT 0 COMMENT '代理端口' AFTER port;
UPDATE server
SET proxy_port = port + 10000;

END IF;

-- 检测account表中的status、gid字段是否存在
    IF
NOT EXISTS (
                SELECT 1 FROM information_schema.`COLUMNS` WHERE TABLE_NAME='account' AND COLUMN_NAME='status')
    THEN

ALTER TABLE account
    ADD COLUMN status int(1) DEFAULT 0 COMMENT '是否在线' AFTER memo;

END IF;

-- 检测account表中的status、role_gid字段是否存在
    IF
NOT EXISTS (
                SELECT 1 FROM information_schema.`COLUMNS` WHERE TABLE_NAME='account' AND COLUMN_NAME='role_gid'
            )THEN

ALTER TABLE account
    ADD COLUMN role_gid varchar(128) DEFAULT '' COMMENT '角色GID' AFTER status;

END IF;

END $$

delimiter ;

CALL PRO_ADD();
DROP PROCEDURE IF EXISTS PRO_ADD;


SET FOREIGN_KEY_CHECKS = 1;
