#!/bin/bash +x

if [ $# != 6 ]; then
  echo '传入参数错误，参数顺序参考：服务端文件根目录 旧的AAA端口 新的AAA端口 数据库用户名 数据库密码 数据库IP 例如：sh exec.sh /asktao/ 8101 8100 root 123456 127.0.0.1'
  exit
fi

basePath=$1
oldPort=$2
newPort=$3
user=$4
password=$5
dbIP=$6

list=$(find $basePath -type f -name "*.ini" | xargs grep "$oldPort")
for file in $list; do
  array=(${file//:/ })
  path=${array[0]}
  echo "修改：$path"
  sed -i "s/$oldPort/$newPort/" $path
  echo "成功！"
done

mysql -u$user -h$dbIP -p$password -e "UPDATE dl_adb_all.aaa SET port='$newPort' WHERE aaa='aaa_daili'"

echo '修改完成！'
