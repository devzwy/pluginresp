#!/bin/bash +x
#===============================================用户手动修改的参数 除了这里的参数，其他都不要动！==========================================================
DB_USERNAME="root"     #数据库用户名
DB_PASSWORD=""         #数据库密码
DB_IP="127.0.0.1"      #数据库IP
DB_PORT="3306"       #数据库端口
SOURE_BIND_PORT="8101" #素材绑定的端口，请确保该端口与游戏服务AAA中的端口不同，否则会导致插件启动失败！
#===============================================用户手动修改的参数 除了这里的参数，其他都不要动！==========================================================

#以下内容不要手动修改

#工作目录

DIR_TEMP_PATH="/asktao"
BASEURL="http://r63cd2q6r.hn-bkt.clouddn.com"
EXEZIP="exe.zip"
PLUGIN_JAR_DOWNLOAD_URL="$BASEURL/plugin.jar"
PLUGIN_SQL_DOWNLOAD_URL="$BASEURL/asktao.sql"
PLUGIN_UPDATESQL_DOWNLOAD_URL="$BASEURL/update.sql"
PLUGIN_VUE_DOWNLOAD_URL="$BASEURL/dist.zip"
PLUGIN_LOGEXE_DOWNLOAD_URL="${BASEURL}/$EXEZIP"
PLUGIN_STARTER_DOWNLOAD_URL="$BASEURL/starter"
PLUGIN_SHELL_DOWNLOAD_URL="$BASEURL/plugin.sh"
AUTOMSG_DOWNLOAD_URL="$BASEURL/random.content"
AUTONAMES_DOWNLOAD_URL="$BASEURL/random.names"


JAR_FILE_NAME="plugin.jar"
VUE_FILE_NAME="dist.zip"
SQL_FILE_NAME="asktao.sql"

SQL_UPDATE_FILE_NAME="update.sql"
CONFIG_PATH="$DIR_TEMP_PATH/plugin.conf"
DB_BACK_PACK_DIR=${DIR_TEMP_PATH}/DBbackpack

FILE_PATH_AUTO_MSG="${DIR_TEMP_PATH}/random.content"
FILE_PATH_AUTO_NAMES="${DIR_TEMP_PATH}/random.names"
ISWULIJI=false

#正常日志输出，绿色文字
#参数1：日志内容
#参数2：日志输出后延时的时长 单位秒，默认为0
echoTips() {
  echo -e "\033[32m$1\033[0m"

  if [ $2 ] && [ "$2" -gt 0 ]; then
    sleep $2
  fi

}

#异常日志输出，红色文字
#参数1：日志内容
#参数2：日志输出后延时的时长 单位秒，默认为0
echoErrorTips() {
  echo -e "\033[31m $1 \033[0m"

  if [ $2 ] && [ "$2" -gt 0 ]; then
    sleep $2
  fi

}

#显示一个提示消息，询问用户是否继续，当用户确认输入Y/y时程序正常继续。否则将执行exit
showDialog() {
  read -p "$1 【Y/n】:" choose

  if [[ $choose ]] && [[ $choose == 'Y' ]] || [[ $choose == 'y' ]]; then
    return 1
  fi

  exit
}

#安装插件
installPlugin() {

  source $CONFIG_PATH

  SOURE_BIND_PORT=$aaaPort
  DB_USERNAME=$dbUserName
  DB_PASSWORD=$dbPassword
  DB_IP=$dbIp
  DB_PORT=$dbPort


  reslt=$(mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "SELECT port FROM dl_adb_all.aaa;")

  array=(${reslt// / })

  if [ "${array[1]}" == "$SOURE_BIND_PORT" ]; then
      echoErrorTips '代理AAA端口不能与真实AAA端口一致！'
      exit
  fi

  #关闭安全性linux
  sed "s/SELINUX=enforcing/SELINUX=disabled/" /etc/selinux/config

  setenforce 0

  #将最新的shell脚本存入/usr/bin
  wget -N -P /usr/bin $PLUGIN_SHELL_DOWNLOAD_URL

  echoTips '安装插件环境...' 2

  yum install -y epel-release zip unzip java-1.8.0-openjdk.x86_64 java-1.8.0-openjdk-devel.x86_64 java-1.8.0-openjdk-headless.x86_64 lsof redis nginx golang

  if [ ! $? = 0 ]; then
    echoErrorTips '插件环境安装失败，请联系作者！'
    exit
  fi

  #这里重复安装一下这两个服务，发现部分服务器第一次安装的时候不会安装成功
  yum install -y redis nginx

  systemctl enable redis
  systemctl start redis

  #替换Nginx配置文件
  if [ `grep -c "prod-api" /etc/nginx/nginx.conf` -eq '0' ];then

    mod="server {\nlisten 1024;\nserver_name 127.0.0.1;\nlocation / {\nroot /asktao/dist;\nindex index.html;\ntry_files \$uri \$uri/ /index.html;\n}\n\nlocation /prod-api/ {  # 反向代理到后端工程\nproxy_set_header Host \$http_host;\nproxy_set_header X-Real-IP \$remote_addr;\nproxy_set_header REMOTE-HOST \$remote_addr;\nproxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;\nproxy_pass http://127.0.0.1:10086/;\n}\nerror_page   500 502 503 504  /50x.html;\nlocation = /50x.html {\nroot   html;\n}\n}\n"
    sed -i "/#    server {/i $mod" /etc/nginx/nginx.conf

    nginx -s reload

    systemctl enable nginx

    systemctl start nginx
  fi

  #临时文件夹是否存在，不存在时创建
  if [ ! -d $DIR_TEMP_PATH ]; then
    log '创建临时文件夹...'
    mkdir -p $DIR_TEMP_PATH
  fi

  cd $DIR_TEMP_PATH || exit

  echoTips '下载并导入数据库...'

  wget -N $PLUGIN_SQL_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '插件SQL下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  chmod +x $DIR_TEMP_PATH/$SQL_FILE_NAME

  #替换aaa中转端口
  sed -i "s/SOURE_BIND_PORT/$SOURE_BIND_PORT/" $DIR_TEMP_PATH/$SQL_FILE_NAME

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD <$DIR_TEMP_PATH/$SQL_FILE_NAME

  mysqlshow -h$DB_IP -P$DB_PORT  --user=$DB_USERNAME --password=$DB_PASSWORD asktao >/etc/null 2>&1

  if [ ! $? = 0 ]; then
    echoErrorTips '数据库导入失败，请重试！'
    exit
  fi

  echoTips '前置工作完成，正在下载其他所需文件...' 2

  if [ ! -f $FILE_PATH_AUTO_MSG ]; then
      wget -N $AUTOMSG_DOWNLOAD_URL
      if [ ! $? = 0 ]; then
        echoErrorTips '下载自动喊话文件失败，网络不稳定，请稍后重试！'
        exit
      fi
  fi

  if [ ! -f $FILE_PATH_AUTO_NAMES ]; then
      wget -N $AUTONAMES_DOWNLOAD_URL
      if [ ! $? = 0 ]; then
        echoErrorTips '下载自动喊话昵称文件失败，网络不稳定，请稍后重试！'
        exit
      fi
  fi

  wget -N $PLUGIN_VUE_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '插件页面下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  # shellcheck disable=SC2115
  rm -rf /usr/bin/$PLUGIN_STARTER_DOWNLOAD_URL

  wget -N -P /usr/bin $PLUGIN_STARTER_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '启动器下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  chmod 0777 /usr/bin/starter

  wget -N -P $DIR_TEMP_PATH $PLUGIN_LOGEXE_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '登录器模版下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  echo A | unzip $DIR_TEMP_PATH/$EXEZIP

  # shellcheck disable=SC2115
  rm -rf "$DIR_TEMP_PATH/$EXEZIP"

  echo A | unzip $DIR_TEMP_PATH/$VUE_FILE_NAME

  systemctl restart nginx

  echoTips '下载并启动插件...'

  wget -N $PLUGIN_JAR_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '插件下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  jar -xvf $DIR_TEMP_PATH/$JAR_FILE_NAME BOOT-INF/classes/application-release.yml

  sed -i "s/REPLACE_DB_PASSWORD/$DB_PASSWORD/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_USER/$DB_USERNAME/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_WULIJI/$ISWULIJI/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_IP/$DB_IP/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_PORT/$DB_PORT/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml

  jar uvf $DIR_TEMP_PATH/$JAR_FILE_NAME BOOT-INF/classes/application-release.yml

  cd $DIR_TEMP_PATH && nohup starter /usr/bin/java -jar $DIR_TEMP_PATH/$JAR_FILE_NAME >/dev/null 2>&1 &

  status=$(systemctl status firewalld)

  result=$(echo $status | grep "Active: active (running)")

  if [ "$result" != "" ]; then
    echoTips '防火墙开启状态，正在放行所需端口...'
    isOpenPort=$(firewall-cmd --list-port | tr ' ' '\n' | grep '1024/tcp')

    if [ ! $isOpenPort ]; then
      echoTips '放行1024端口...'
      firewall-cmd --zone=public --add-port=1024/tcp --permanent
      firewall-cmd --reload
    fi

    isOpenPort=$(firewall-cmd --list-port | tr ' ' '\n' | grep '10086/tcp')

    if [ ! $isOpenPort ]; then
      echoTips '放行10086端口...'
      firewall-cmd --zone=public --add-port=10086/tcp --permanent
      firewall-cmd --reload
    fi

    isOpenPort=$(firewall-cmd --list-port | tr ' ' '\n' | grep "$SOURE_BIND_PORT/tcp")

    if [ ! $isOpenPort ]; then
      echoTips "放行${SOURE_BIND_PORT}端口..."
      firewall-cmd --zone=public --add-port=$SOURE_BIND_PORT/tcp --permanent
      firewall-cmd --reload
    fi

    reslt=$(mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "SELECT proxy_port FROM dl_adb_all.server;")

    array=(${reslt// / })

    # shellcheck disable=SC2068
    for var in ${array[@]}
    do
       if [ "$var" -gt 0 ] 2>/dev/null ;then

          isOpenPort=$(firewall-cmd --list-port | tr ' ' '\n' | grep "$var/tcp")

          if [ ! $isOpenPort ]; then
             echoTips "放行${var}端口..."
             firewall-cmd --zone=public --add-port=$var/tcp --permanent
             firewall-cmd --reload
          fi
       fi
    done
  fi

  hostName_=$(hostname)
  sed -i '/^\s*$/d' /etc/hosts
  sudo sed -i "s/$/& $hostName_/g" /etc/hosts

  echoTips '移除临时文件...'
  rm -rf $DIR_TEMP_PATH/BOOT-INF

  rm -rf $DIR_TEMP_PATH/$VUE_FILE_NAME
  rm -rf $DIR_TEMP_PATH/$SQL_FILE_NAME

  echoTips '插件加入开机自启动...'
  echo "#!/bin/bash +x
      sleep 10
      sudo sh plugin.sh --start
    " >/usr/bin/pluginStarter

  echo 'sh /usr/bin/pluginStarter' >>/etc/rc.local

  chmod 0777 /usr/bin/pluginStarter

  echoTips "恭喜您部署成功!插件在后台启动，启动时长与服务器配置有关。大约耗时20-50秒。启动日志在/asktao/logs/sys-info.log，请一直重复打开该文件，直到文件内容输出启动成功与版本号字样即可在浏览器输入【游戏IP:1024】打开插件后台(请注意服务器安全组放行1024、10086端口)，默认用户名为：admin 默认密码为123456，请登录后立即点击右上角头像修改密码，否则造成损失与作者无关！祝您使用愉快！"
}

#更新插件
updatePlugin() {
  source $CONFIG_PATH

  SOURE_BIND_PORT=$aaaPort
  DB_USERNAME=$dbUserName
  DB_PASSWORD=$dbPassword
  DB_IP=$dbIp
  DB_PORT=$dbPort
  ISWULIJI=$iswlj

  if [ ! -n "$ISWULIJI" ]; then
      ISWULIJI=false
  fi

  sed -i "s/proxy_pass https:/proxy_pass http:/" /etc/nginx/nginx.conf

  #修改逻辑 先下载，然后删除原来的
  echoTips '拉取最新脚本...'

  wget -N -P /usr/bin $PLUGIN_SHELL_DOWNLOAD_URL

  cd $DIR_TEMP_PATH || exit

  if [ ! -f $FILE_PATH_AUTO_MSG ]; then
      echoTips '喊话文件丢失，开始下载...'
      wget -N $AUTOMSG_DOWNLOAD_URL
      if [ ! $? = 0 ]; then
        echoErrorTips '下载失败，网络不稳定，请稍后重试！'
        exit
      fi
  fi

  if [ ! -f $FILE_PATH_AUTO_NAMES ]; then
      echoTips '昵称文件丢失，开始下载...'
      wget -N $AUTONAMES_DOWNLOAD_URL
      if [ ! $? = 0 ]; then
        echoErrorTips '下载失败，网络不稳定，请稍后重试！'
        exit
      fi
  fi

  echoTips '更新中...'

  wget -N $PLUGIN_VUE_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips 'UI文件下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  rm -rf $DIR_TEMP_PATH/dist

  echo A | unzip $DIR_TEMP_PATH/$VUE_FILE_NAME

  systemctl restart nginx

  wget  -N $PLUGIN_UPDATESQL_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    rm -rf $DIR_TEMP_PATH/$VUE_FILE_NAME
    echoErrorTips 'SQL下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  chmod +x $DIR_TEMP_PATH/$SQL_UPDATE_FILE_NAME

  sed -i "s/SOURE_BIND_PORT/$SOURE_BIND_PORT/" $DIR_TEMP_PATH/$SQL_UPDATE_FILE_NAME
  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD <$DIR_TEMP_PATH/$SQL_UPDATE_FILE_NAME

  if [ ! $? = 0 ]; then
    echoErrorTips '数据库导入失败，请重试！'
    exit
  fi

  wget -N -P $DIR_TEMP_PATH $PLUGIN_LOGEXE_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '登录器模版下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  echo A | unzip $DIR_TEMP_PATH/$EXEZIP

  # shellcheck disable=SC2115
  rm -rf "$DIR_TEMP_PATH/$EXEZIP"

  #下载最新插件
  wget -N $PLUGIN_JAR_DOWNLOAD_URL

  if [ ! $? = 0 ]; then
    echoErrorTips '插件下载失败，网络不稳定，请稍后重试！'
    exit
  fi

  #如果插件开启状态时关闭后再更新
  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')

  if [ $port ]; then
    echoTips '正在停止插件(游戏将掉线，启动大约耗时20-50秒，插件掉线不会回档！)...' 1
    kill -9 $port
    echoTips '...' 2
  fi

  rm -rf $DIR_TEMP_PATH/logs

  jar -xvf $DIR_TEMP_PATH/$JAR_FILE_NAME BOOT-INF/classes/application-release.yml

  sed -i "s/REPLACE_DB_PASSWORD/$DB_PASSWORD/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_USER/$DB_USERNAME/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_WULIJI/$ISWULIJI/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_IP/$DB_IP/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml
  sed -i "s/REPLACE_DB_PORT/$DB_PORT/" $DIR_TEMP_PATH/BOOT-INF/classes/application-release.yml

  jar uvf $DIR_TEMP_PATH/$JAR_FILE_NAME BOOT-INF/classes/application-release.yml

  cd $DIR_TEMP_PATH && nohup starter /usr/bin/java -jar $DIR_TEMP_PATH/$JAR_FILE_NAME >/dev/null 2>&1 &

  rm -rf $DIR_TEMP_PATH/BOOT-INF

  echoTips '恭喜您更新成功!插件在后台启动，启动时长与服务器配置有关。大约耗时20-50秒。启动日志在/asktao/logs/，请一直重复打开sys-info日志，直到文件内容输出启动成功与版本号字样即可访问后台页面，祝您使用愉快！'

}

#启动插件
startPlugin() {
  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')
  if [ $port ]; then
    echoTips '插件已启动，请勿多次操作！'
    exit
  fi

  cd $DIR_TEMP_PATH && nohup starter /usr/bin/java -jar $DIR_TEMP_PATH/$JAR_FILE_NAME >/dev/null 2>&1 &
  echoTips '启动命令执行完成，插件的真实启动与否状态请使用--status查询!'
}

#停止插件
stopPlugin() {
  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')
  if [ $port ]; then
    kill -9 $port
    echoTips '插件已停止!'
    exit
  fi
  echoErrorTips '插件停止失败！请确认插件是否已启动！'
}

##插件状态
#statusPlugin() {
#  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')
#  if [ $port ]; then
#    echoTips '插件已启动，正常运行中...'
#    exit
#  fi
#  echoErrorTips "插件未启动，如果插件为刚启动，则需要等待20-60秒左右才能启动完成！如果启动失败时请查阅日志，日志路径$DIR_TEMP_PATH/pluginLogs"
#}

#重启插件
restartPlugin(){
  echoTips '停止插件...' 2

  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')

  if [ $port ]; then
    kill -9 $port >/dev/null 2>&1 &
  fi

  echoTips '重载Nginx...' 2

  systemctl restart nginx

  echoTips '重载Redis...' 2

  systemctl restart redis

  cd $DIR_TEMP_PATH && nohup starter /usr/bin/java -jar $DIR_TEMP_PATH/$JAR_FILE_NAME >/dev/null 2>&1 &

  echoTips '重启完成!插件在后台启动，启动时长与服务器配置有关。大约耗时20-50秒。启动日志在/asktao/logs/，请一直重复打开sys-info日志，直到文件内容输出启动成功与版本号字样即可访问后台页面，祝您使用愉快！'
}

#删除插件
removePlugin() {
  showDialog '是否删除插件？'

  source $CONFIG_PATH

  SOURE_BIND_PORT=$aaaPort
  DB_USERNAME=$dbUserName
  DB_PASSWORD=$dbPassword
  DB_IP=$dbIp
  DB_PORT=$dbPort

  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')
  if [ $port ]; then
    echoTips '正在停止插件...'
    kill -9 $port
  fi

  echoTips '停止服务...'

  systemctl stop redis
  systemctl stop nginx

  echoTips '开始删除插件环境...' 1
  yum remove -y redis nginx

  echoTips '开始移除文件...'

  rm -rf $DIR_TEMP_PATH/$JAR_FILE_NAME
  rm -rf $DIR_TEMP_PATH/$VUE_FILE_NAME
  rm -rf $DIR_TEMP_PATH/$SQL_FILE_NAME
  rm -rf $DIR_TEMP_PATH/dist
  rm -rf $DIR_TEMP_PATH/login.exe
  rm -rf /usr/bin/starter
  rm -rf $DIR_TEMP_PATH/logs
  rm -rf $CONFIG_PATH
  rm -rf /usr/bin/pluginStarter
  rm -rf /usr/bin/plugin.sh

  sed -i "/pluginStarter/d" /etc/rc.local

  echoTips '删除成功，欢迎您再次使用！'
}

backpackDB(){

  dirName=`(date '+%Y-%m-%d(%H:%M:%S)')`

   if [ ! -d $DB_BACK_PACK_DIR  ];then
     mkdir $DB_BACK_PACK_DIR
   fi

   if [ ! -d $DB_BACK_PACK_DIR/$dirName  ];then
     mkdir $DB_BACK_PACK_DIR/$dirName
   fi

   source $CONFIG_PATH

   SOURE_BIND_PORT=$aaaPort
   DB_USERNAME=$dbUserName
   DB_PASSWORD=$dbPassword
   DB_IP=$dbIp
   DB_PORT=$dbPort
   echo '开始备份...'

   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD asktao > $DB_BACK_PACK_DIR/$dirName/asktao.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_adb_all > $DB_BACK_PACK_DIR/$dirName/dl_adb_all.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_ddb_1 > $DB_BACK_PACK_DIR/$dirName/dl_ddb_1.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_dmdb_1 > $DB_BACK_PACK_DIR/$dirName/dl_dmdb_1.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_ldb_1 > $DB_BACK_PACK_DIR/$dirName/dl_ldb_1.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_ldb_all > $DB_BACK_PACK_DIR/$dirName/dl_ldb_all.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_mdb_1 > $DB_BACK_PACK_DIR/$dirName/dl_mdb_1.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_mdb_all > $DB_BACK_PACK_DIR/$dirName/dl_mdb_all.sql
   mysqldump -u$DB_USERNAME -P$DB_PORT -p$DB_PASSWORD dl_tdb_1 > $DB_BACK_PACK_DIR/$dirName/dl_tdb_1.sql

   echo '备份完成'$DB_BACK_PACK_DIR/$dirName
}

importData(){
  showDialog "友情提醒,清理垃圾后再恢复数据将会使恢复时间缩短一万倍哦！请将备份文件放在${DB_BACK_PACK_DIR}下后继续操作！（将需要恢复的sql文件全部放到该目录下）"
  source $CONFIG_PATH

  SOURE_BIND_PORT=$aaaPort
  DB_USERNAME=$dbUserName
  DB_PASSWORD=$dbPassword
  DB_IP=$dbIp
  DB_PORT=$dbPort

  echoTips '恢复 dl_adb_all ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_adb_all DEFAULT CHARACTER SET latin1;use dl_adb_all;set names latin1;source $DB_BACK_PACK_DIR/dl_adb_all.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_adb_all数据库恢复成功！'
    else
      echoErrorTips 'dl_adb_all数据库恢复失败！'
  fi

  echoTips '恢复 dl_ddb_1 ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_ddb_1 DEFAULT CHARACTER SET latin1;use dl_ddb_1;set names latin1;source $DB_BACK_PACK_DIR/dl_ddb_1.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_ddb_1数据库恢复成功！'
    else
      echoErrorTips 'dl_ddb_1数据库恢复失败！'
  fi

  echoTips '恢复 dl_dmdb_1 ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_dmdb_1 DEFAULT CHARACTER SET latin1;use dl_dmdb_1;set names latin1;source $DB_BACK_PACK_DIR/dl_dmdb_1.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_dmdb_1数据库恢复成功！'
    else
      echoErrorTips 'dl_dmdb_1数据库恢复失败！'
  fi

  echoTips '恢复 dl_ldb_1 ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_ldb_1 DEFAULT CHARACTER SET latin1;use dl_ldb_1;set names latin1;source $DB_BACK_PACK_DIR/dl_ldb_1.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_ldb_1数据库恢复成功！'
    else
      echoErrorTips 'dl_ldb_1数据库恢复失败！'
  fi

  echoTips '恢复 dl_ldb_all ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_ldb_all DEFAULT CHARACTER SET latin1;use dl_ldb_all;set names latin1;source $DB_BACK_PACK_DIR/dl_ldb_all.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_ldb_all数据库恢复成功！'
    else
      echoErrorTips 'dl_ldb_all数据库恢复失败！'
  fi


  echoTips '恢复 dl_mdb_1 ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_mdb_1 DEFAULT CHARACTER SET latin1;use dl_mdb_1;set names latin1;source $DB_BACK_PACK_DIR/dl_mdb_1.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_mdb_1数据库恢复成功！'
    else
      echoErrorTips 'dl_mdb_1数据库恢复失败！'
  fi

  echoTips '恢复 dl_mdb_all ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_mdb_all DEFAULT CHARACTER SET latin1;use dl_mdb_all;set names latin1;source $DB_BACK_PACK_DIR/dl_mdb_all.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_mdb_all数据库恢复成功！'
    else
      echoErrorTips 'dl_mdb_all数据库恢复失败！'
  fi

  echoTips '恢复 dl_tdb_1 ...'

  mysql -h$DB_IP -P$DB_PORT -u$DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS dl_tdb_1 DEFAULT CHARACTER SET latin1;use dl_tdb_1;set names latin1;source $DB_BACK_PACK_DIR/dl_tdb_1.sql;" >/dev/null 2>&1

  if [ $? == 0 ]; then
      echoTips 'dl_tdb_1数据库恢复成功！'
    else
      echoErrorTips 'dl_tdb_1数据库恢复失败！'
  fi

  echoTips '数据恢复完成！'
}

#检测
checkStatus(){
  clear
  echoTips '====================================================开始检测==============================================================='
  sleep 1
  index=0
  status=$(systemctl status redis)

  result=$(echo $status | grep "Active: active (running)")

  if [ "$result" == "" ]; then
      echoErrorTips '检测到Redis服务未启动，插件异常状态，请重新更新或卸载插件重新安装！'
    else
      index=`expr $index + 1`
      echoTips 'Redis服务运行正常...'
  fi


  status=$(systemctl status nginx)

  result=$(echo $status | grep "Active: active (running)")

  if [ "$result" == "" ]; then
      echoErrorTips '检测到Nginx服务未启动，插件异常状态，请重新更新或卸载插件重新安装！'
    else
      index=`expr $index + 1`
      echoTips 'Nginx服务运行正常...'
  fi

  port=$(netstat -nlp | grep :10086 | awk '{print $7}' | awk -F"/" '{ print $1 }')
  if [ $port ]; then
      index=`expr $index + 1`
      echoTips '插件运行正常'
    else
      echoTips '插件未运行...'
  fi

  status=$(systemctl status mysqld)

  result=$(echo $status | grep "Active: active (running)")

  if [ "$result" == "" ]; then
      echoErrorTips '检测到MySQL服务未启动，请确认游戏数据库是否正常或插件数据库asktao是否存在！建议重新架设！'
    else
      index=`expr $index + 1`
      echoTips 'MySQL服务运行正常...'
  fi


  if [ $index == 4 ]; then
        echoTips '插件正常运行...'
      else
        echoErrorTips '插件异常！请按照提示操作！'
  fi

  sleep 1
  echoTips '=====================================================检测结束=============================================================='
}

clear

#程序入口
case $1 in

  --install) #安装插件

    if [ $# != 5 ] && [ $# != 7 ]; then
      echoErrorTips '安装命令错误'
      exit
    fi

    DB_USERNAME=$2
    DB_PASSWORD=$3
    SOURE_BIND_PORT=$4
    if [ $5 == 0 ]; then
        ISWULIJI=false
      else
        ISWULIJI=true
    fi


    if [ $# = 7 ]; then
      DB_IP=$5
      DB_PORT=$6
      if [ $7 == 0 ]; then
          ISWULIJI=false
        else
          ISWULIJI=true
      fi
    fi

    showDialog "请确认资料是否正确，错误的数据库资料将导致插件安装失败！是否物理机：$ISWULIJI 数据库用户名:$DB_USERNAME,数据库密码:$DB_PASSWORD,数据库IP:$DB_IP,素材绑定的端口:$SOURE_BIND_PORT,数据库端口：$DB_PORT"

echo "
aaaPort=$SOURE_BIND_PORT
dbUserName=$DB_USERNAME
dbPassword=$DB_PASSWORD
dbIp=$DB_IP
dbPort=$DB_PORT
iswlj=$ISWULIJI
" > $CONFIG_PATH

    installPlugin

    ;;

  --update) #更新插件
    updatePlugin
    ;;

  --remove) #删除插件
    removePlugin
    ;;
  --start) #启动插件
    startPlugin
    ;;
  --stop) #停止插件
    stopPlugin
    ;;

  --restart) #重启插件
    restartPlugin
    ;;
  --status) #插件状态
    checkStatus
    ;;

  --backup) #备份数据库
    backpackDB
    ;;
  --importData)
    importData
    ;;

  --disabledSELINUX)
    showDialog '是否关闭SELINUX，关闭后可避免插件重启后无法启动的问题，如果已经关闭过请勿再次关闭'
    echoTips '正在关闭...'
    setenforce 0
    sed -i "s/SELINUX=enforcing/SELINUX=disabled/" /etc/sysconfig/selinux
    echoTips '关闭成功，需要重启生效！'
    ;;


  --addswap) #开辟虚拟内存

    if [ $# != 2 ]; then
      echoErrorTips "虚拟空间大小不能为空，可使用服务器物理内存*1.5或者2的值取整，比如物理4G内存，那就写6,2G就写3,8就写16..."
      exit
    fi

    size=$2

    echoTips "正在分配虚拟内存(${size}G),数值越大耗时越久，请耐心等待..."
    #创建交换空间
    dd if=/dev/zero of=/root/swapfile_asktao bs=${size}M count=1024

    echoTips '授权虚拟空间...'

    #授权
    chmod 0777 /root/swapfile_asktao

    echoTips '格式化虚拟空间...'
    #格式化为虚拟内存空间
    mkswap /root/swapfile_asktao

    echoTips '启用虚拟内存...'
    #启用虚拟内存
    swapon /root/swapfile_asktao

    echoTips '加入开机自启动...'
    #加入开机自启动
    echo  "
swapon /root/swapfile_asktao" >> /etc/rc.local

    echoTips "虚拟内存开启完成，本次开启${size}G虚拟内存，重启电脑后生效！"
    ;;

  --removeswap) #移除虚拟内存

    echoTips "正在移除，请耐心等待..."

    swapoff /root/swapfile_asktao

    rm -rf /root/swapfile_asktao

    sed -i 's/swapon \/root\/swapfile_asktao/ /' /etc/rc.local

    echoTips "虚拟内存已删除，重启电脑后生效！"
    ;;

#  --checkStatus) #检查状态
#    checkStatus
#    ;;

    *)


    echoErrorTips '不支持的指令'
    ;;
esac
